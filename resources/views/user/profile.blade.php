@extends('layouts.backend.app')

@section('title','Profile')
@push('css')
    <!-- data tables -->
    <link href="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- sweetalert2-->
    {{--<link href="{{ asset('assets/backend/css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">User Profile</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('admin.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        </li>
                        <li class="active">User Profile</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar offset-4">
                        <div class="card card-topline-aqua">
                            <div class="card-body no-padding height-9">
                                <div class="row">
                                    <div class="profile-userpic">
                                        <img src=" {{ asset('storage/profile/'.Auth::user()->image) }}" class="img-responsive" alt=""> </div>
                                </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">{{ Auth::user()->name }} </div>
                                    <div class="profile-usertitle-job">{{ Auth::user()->role->role_name }} </div>
                                </div>
                                <div class="card-body no-padding height-9">
                                    <ul class="list-group list-group-unbordered">
                                        <table class="table">
                                            <tr>
                                                <td><b>Email :</b></td>
                                                <td>{{ Auth::user()->email }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Phone :</b></td>
                                                <td>+880{{ Auth::user()->phone }}</td>
                                            </tr>
                                        </table>
                                    </ul>

                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                        </div>
                    </div>
                    <!-- end page content -->
                </div>
                <!-- end page container -->
@endsection

            @push('js')
                <!-- data tables -->
                    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}" ></script>
                    <script src="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}" ></script>
                    <script src="{{ asset('backend/assets/js/pages/table/table_data.js') }}" ></script>
                    {{--<script src="{{ asset('https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.js') }}" ></script>--}}
                    <script src="{{ asset('backend/assets/js/sweetalert2.min.js') }}" ></script>
                    <script type="text/javascript">
                        function deletePost(id) {
                            swal({
                                title: 'Are you sure?',
                                text: "You won't be able to revert this!",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, delete it!',
                                cancelButtonText: 'No, cancel!',
                                confirmButtonClass: 'btn btn-success',
                                cancelButtonClass: 'btn btn-danger',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    event.preventDefault();
                                    document.getElementById('delete-form-'+id).submit();
                                } else if (
                                    // Read more about handling dismissals
                                    result.dismiss === swal.DismissReason.cancel
                                ) {
                                    swal(
                                        'Cancelled',
                                        'Your data is safe :)',
                                        'error'
                                    )
                                }
                            })
                        }
                    </script>
    @endpush
