@extends('layouts.backend.app')

@section('title','User-Dashboard')
@push('css')
    <!-- data tables -->
    <link href="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <!--sweetalert2 -->
    <link href="{{ asset('backend/assets/css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
    {{--<link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">--}}
@endpush

@section('content')

@endsection

@push('js')
    <!-- counterup js -->
    <script src="{{ asset('backend/assets/plugins/counterup/jquery.waypoints.min.js') }}" ></script>
    <script src="{{ asset('backend/assets/plugins/counterup/jquery.counterup.min.js') }}" ></script>

    <!-- data tables -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}" ></script>
    <script src="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}" ></script>
    <script src="{{ asset('backend/assets/js/pages/table/table_data.js') }}" ></script>
    <script src="{{ asset('https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.js') }}" ></script>

    <script type="text/javascript">
        function deletePost(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>

@endpush
