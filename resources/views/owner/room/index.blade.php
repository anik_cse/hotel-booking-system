@extends('layouts.backend.app')
@section('title','Room List')
@push('css')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Room List</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('owner.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="{{ route('owner.room.index') }}">Room</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Room List</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-line">
                    <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tab1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-box">
                                        <div class="card-head">
                                            <header>Room List <span class="badge bg-info">{{ $rooms->count() }}</span></header>

                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-6">
                                                    <div class="btn-group">

                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-6">

                                                </div>
                                            </div>
                                            <div class="table-scrollable">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example4">
                                                    <thead>
                                                    <tr class="center">
                                                        <th>#</th>
                                                        <th>Image </th>
                                                        {{--<th>Owner Name </th>--}}
                                                        <th>Hotel Name</th>
                                                        <th>Room Name</th>
                                                        <th>Rant Price</th>
                                                        <th>Sale Rant Price</th>
                                                        <th>Check-in</th>
                                                        <th>Check-out</th>
                                                        <th>Booking Status</th>
                                                        <th>Action </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($rooms as $key=>$room)
                                                        <tr class="odd gradeX">

                                                            <td>{{ $key+1 }}</td>
                                                            <td class="center">
                                                                @php
                                                                    $image = DB::table('room_image')->where('room_id',$room->id)->first();

                                                                @endphp
                                                                <img src="{{ asset('storage/room/'.$image->image) }}" alt="hotel image" width="50px" height="50px">
                                                            </td>
                                                            {{--<td class="center">--}}
                                                                {{--@php--}}
                                                                    {{--$owner = DB::table('users')->where('id',$room->user_id)->first();--}}

                                                                {{--@endphp--}}
                                                                {{--{{ $room->user_id }}--}}
                                                            {{--</td>--}}
                                                            <td class="center">
                                                                @php
                                                                    $hotel = DB::table('hotels')->where('id',$room->hotel_id)->first();
                                                                @endphp
                                                                {{ $hotel->hotel_name }}
                                                            </td>
                                                            <td class="center">{{ $room->room_name }}</td>
                                                            @if($room->booking_sale_price == NULL)
                                                                <td class="center">{{ $room->booking_price }}</td>
                                                            @else
                                                            <td class="center"><del>{{ $room->booking_price }}</del></td>
                                                            @endif
                                                            <td class="center">{{ $room->booking_sale_price }}</td>
                                                            <td class="center">
                                                                @if($room->booking_start == NULL)

                                                                @else
                                                                    {{date('d M y', strtotime($room->booking_start))}}
                                                                @endif
                                                            </td>
                                                            <td class="center">
                                                                @if($room->booking_start == NULL)

                                                                @else
                                                                    {{date('d M y', strtotime($room->booking_end))}}
                                                                @endif
                                                            </td>
                                                            <td class="center">
                                                                @if($room->booking_status == "0")
                                                                    <span style="color: green; font-weight: bold">Free</span>
                                                                @else
                                                                    <span style="color: red;font-weight: bold">Booked</span>
                                                                @endif
                                                            </td>



                                                            <td class="center">
                                                                @if($room->booking_status == "1")
                                                            <form action="{{ route('owner.room-free') }}" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="room_id" value="{{ $room->id }}"/>
                                                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i></button>
                                                            </form>
                                                                @else
                                                                <button data-toggle="modal" data-target="#Edit" data-id="{{ $room->id }}" data-hotelname="{{ $room->hotel_id}}" data-name="{{ $room->room_name }}"  data-size="{{ $room->room_size }}" data-sleep="{{ $room->sleep }}" data-booking_price="{{ $room->booking_price }}" data-booking_sale_price="{{ $room->booking_sale_price }}" data-is_featured="{{ $room->is_featured }}" data-qty="{{ $room->qty }}" data-status="{{ $room->booking_status }}" class="btn btn-info btn-xs"> <i class="fa fa-calendar-plus-o" aria-hidden="true"></i></button>
                                                                @endif

                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Booking information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('owner.room.update','id') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="check_id">Check-in</label>
                                        <input type="hidden" name="id"  class="form-control" id="Rid"/>
                                        <input type="text" id="check_in" name="check_in" value="{{ date('Y-m-d') }}" class="form-control" id="check_in" autocomplete="off"/>
                                        <script>
                                            $('#check_in').datepicker({
                                                format: 'yyyy-mm-dd',
                                                autoclose: false,
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rSize">Check-out</label>
                                        <input type="text" name="check_out" value="{{ date('Y-m-d') }}" class="form-control" id="check_out" autocomplete="off"/>
                                        <script>
                                            $('#check_out').datepicker({
                                                format: 'yyyy-mm-dd',
                                                autoclose: false,
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Booking</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script type="text/javascript">
        $('#Edit').on('show.bs.modal', function (event) {
            var button          = $(event.relatedTarget);
            var roomId          = button.data('id');


            var modal = $(this);

            modal.find('.modal-body #Rid').val(roomId);




        });
        function deleteHotel(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }

    </script>

@endpush
