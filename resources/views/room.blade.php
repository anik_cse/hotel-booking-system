@extends('layouts.app')
@section('title',$roomDetails->room_name)
@push('css')
    <style>
        .product-gallery.on-show-slider #sync1 img{
            height: 500px;
        }
        fieldset { border: 0; }
    </style>
    <!-- Revolution Slider Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('fontend/assets/plugins/revolution/revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fontend/assets/plugins/revolution/revolution/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fontend/assets/plugins/revolution/revolution/css/navigation.css') }}">
    <!-- Revolution Navigation Style -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <div class="content-block">
            <div class="section-full content-inner">
                <div class="container">
                    <div class="row m-b30">
                        <div class="col-lg-12">
                            <div class="product-gallery on-show-slider">
                                <div id="sync1" class="owl-carousel owl-theme owl-btn-center-lr m-b5 owl-btn-1 primary">
                                   @foreach($roomImages as $image)
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/room/'.$image->image) }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <div id="sync2" class="owl-carousel owl-theme owl-none">
                                    @foreach($roomImages as $image)
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/room/'.$image->image) }}" alt="">
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tour-days">
                                <h2 class="m-b10">{{ $roomDetails->room_name }}</h2>
                                <i class="fa fa-home" aria-hidden="true"> </i> {{ $hotelDetails->hotel_name }}
                                <p><i class="fa fa-map-marker m-r5"></i>{{ $hotelDetails->city }}, {{ $hotelDetails->zillas }},{{ $hotelDetails->divisions }} </p>
                            </div>

                            <div class="room_facilities">
                                <h6>Room facilities:</h6>
                                <div class="row">
                                    @foreach($facilities as $faci)
                                        @php
                                        $faci_name = DB::table('facilities')->where('id',$faci->facilities_id)->get();
                                        @endphp
                                        @foreach($faci_name as $f_name)
                                            <div class="col-md-2"><p> <i class="fa fa-check-circle-o" aria-hidden="true"></i> {{ $f_name->name }}</p></div>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                            <div class="booking_now" >
                                <form action="{{ route('booking') }}" method="POST">
                                    @csrf
                                    <div class="row" style="margin-left: 10px;margin-top: 5px;">
                                        <div class="form-group col-md-4" >
                                            <label>Check-in</label>
                                            <input type="text" id="check_in" name="check_in" value="{{ date('Y-m-d') }}" class="form-control" autocomplete="off"/>
                                            <script>
                                                $('#check_in').datepicker({
                                                    format: 'yyyy-mm-dd',
                                                    autoclose: false,
                                                });
                                            </script>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="check_out">Check-out</label>
                                            <input type="text" id="check_out" name="check_out" value="{{ date('Y-m-d') }}" class="form-control"  autocomplete="off"/>
                                            <script>
                                                $('#check_out').datepicker({
                                                    format: 'yyyy-mm-dd',
                                                    autoclose: false,
                                                });
                                            </script>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="qty">Room Quantity</label>
                                            <select id="qty" class="form-control" name="qty">
                                                @for ($i = "1"; $i <= $roomDetails->qty; $i++)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left: 10px;margin-top: 5px;">
                                    <div class="form-group col-md-3 col-lg-3 col-sm-3">
                                        <label for="username">Name</label>
                                        <input type="text" class="form-control" name="username" id="username" placeholder="Name"/>
                                        <input type="hidden" class="form-control" value="{{ $roomDetails->id }}" name="room_id" />

                                    </div>
                                    <div class="form-group col-md-3 col-lg-3 col-sm-3">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="email@example.com"/>
                                    </div>
                                    <div class="form-group col-md-3 col-lg-3 col-sm-3">
                                        <label for="phone">Phone</label>
                                        <input type="text" class="form-control" name="phone" pattern="[0-9]{11}" id="phone" placeholder="01234567890"/>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 35px">
                                        <button class="btn btn-success pull-right">Booking Now</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>

        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->
@endsection

@push('js')
    <!-- REVOLUTION JS FILES -->
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
        <!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/plugins/revolution/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        <script src="{{ asset('fontend/assets/js/rev.slider.js') }}"></script>
    <script>
        $(document).ready(function() {

            var sync1 = $("#sync1");
            var sync2 = $("#sync2");
            var slidesPerPage = 8; //globaly define number of elements per page
            var syncedSecondary = true;

            sync1.owlCarousel({
                items : 1,
                slideSpeed : 2000,
                nav: true,
                autoplay: false,
                dots: false,
                loop: true,
                responsiveRefreshRate : 200,
                navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
            }).on('changed.owl.carousel', syncPosition);

            sync2.on('initialized.owl.carousel', function () {
                sync2.find(".owl-item").eq(0).addClass("current");
            }).owlCarousel({
                items : slidesPerPage,
                dots: false,
                nav: false,
                margin:5,
                smartSpeed: 200,
                slideSpeed : 500,
                slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                responsiveRefreshRate : 100
            }).on('changed.owl.carousel', syncPosition2);

            function syncPosition(el) {
                //if you set loop to false, you have to restore this next line
                //var current = el.item.index;

                //if you disable loop you have to comment this block
                var count = el.item.count-1;
                var current = Math.round(el.item.index - (el.item.count/2) - .5);

                if(current < 0) {
                    current = count;
                }
                if(current > count) {
                    current = 0;
                }

                //end block

                sync2
                    .find(".owl-item")
                    .removeClass("current")
                    .eq(current)
                    .addClass("current");
                var onscreen = sync2.find('.owl-item.active').length - 1;
                var start = sync2.find('.owl-item.active').first().index();
                var end = sync2.find('.owl-item.active').last().index();

                if (current > end) {
                    sync2.data('owl.carousel').to(current, 100, true);
                }
                if (current < start) {
                    sync2.data('owl.carousel').to(current - onscreen, 100, true);
                }
            }

            function syncPosition2(el) {
                if(syncedSecondary) {
                    var number = el.item.index;
                    sync1.data('owl.carousel').to(number, 100, true);
                }
            }

            sync2.on("click", ".owl-item", function(e){
                e.preventDefault();
                var number = $(this).index();
                //sync1.data('owl.carousel').to(number, 300, true);

                sync1.data('owl.carousel').to(number, 300, true);

            });
        });
    </script>

@endpush
