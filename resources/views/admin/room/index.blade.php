@extends('layouts.backend.app')
@section('title','Room List')
@push('css')
    <style>
        .wrapper {
            width: 100%;
            height: 300px;
        }
        .drop {
            width: 96%;
            height: 96%;
            border: 3px dashed #DADFE3;
            border-radius: 15px;
            overflow: hidden;
            text-align: center;
            background: white;
            -webkit-transition: all 0.5s ease-out;
            -moz-transition: all 0.5s ease-out;
            transition: all 0.5s ease-out;
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            /*&:hover
             * cursor: pointer
             * background: #f5f5f5 */
        }
        .drop .cont {
            width: 500px;
            height: 170px;
            color: #8E99A5;
            -webkit-transition: all 0.5s ease-out;
            -moz-transition: all 0.5s ease-out;
            transition: all 0.5s ease-out;
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        .drop .cont i {
            font-size: 400%;
            color: #8E99A5;
            position: relative;
        }
        .drop .cont .tit {
            font-size: 400%;
            text-transform: uppercase;
        }
        .drop .cont .desc {
            color: #A4AEBB;
        }
        .drop .cont .browse {
            margin: 10px 25%;
            color: white;
            padding: 8px 16px;
            border-radius: 5px;
            background: #09f;
        }
        .drop input {
            width: 100%;
            height: 100%;
            cursor: pointer;
            background: red;
            opacity: 0;
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        #list {
            width: 100%;
            text-align: left;
            position: absolute;
            left: 0;
            top: 0;
        }
        #list .thumb {
            height: 75px;
            border: 1px solid #323a44;
            margin: 10px 5px 0 0;
        }

    </style>
@endpush

@section('content')
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Room List</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('admin.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="{{ route('admin.room.index') }}">Room</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Room List</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-line">
                    <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tab1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-box">
                                        <div class="card-head">
                                            <header>Room List <span class="badge bg-info">{{ $rooms->count() }}</span></header>

                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-6">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNew">
                                                            Add New <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-6">

                                                </div>
                                            </div>
                                            <div class="table-scrollable">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example4">
                                                    <thead>
                                                    <tr class="center">
                                                        <th>#</th>
                                                        <th>Image </th>
                                                        <th>Hotel Name</th>
                                                        <th>Room Name</th>
                                                        <th>Rant Price</th>
                                                        <th>Sale Rant Price</th>
                                                        <th>Action </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($rooms as $key=>$room)
                                                        <tr class="odd gradeX">

                                                            <td>{{ $key+1 }}</td>
                                                            <td class="center">
                                                                @php
                                                                    $image = DB::table('room_image')->where('room_id',$room->id)->first();

                                                                @endphp
                                                                <img src="{{ asset('storage/room/'.$image->image) }}" alt="hotel image" width="50px" height="50px">
                                                            </td>
                                                            <td class="center">
                                                                @php
                                                                    $hotel = DB::table('hotels')->where('id',$room->hotel_id)->first();
                                                                @endphp
                                                                {{ $hotel->hotel_name }}
                                                            </td>
                                                            <td class="center">{{ $room->room_name }}</td>
                                                            @if($room->booking_sale_price == NULL)
                                                                <td class="center">{{ $room->booking_price }}</td>
                                                            @else
                                                            <td class="center"><del>{{ $room->booking_price }}</del></td>
                                                            @endif
                                                            <td class="center">{{ $room->booking_sale_price }}</td>
                                                            <td class="center">

                                                                <button data-toggle="modal" data-target="#Edit" data-id="{{ $room->id }}" data-hotelname="{{ $room->hotel_id}}" data-name="{{ $room->room_name }}"  data-size="{{ $room->room_size }}" data-sleep="{{ $room->sleep }}" data-booking_price="{{ $room->booking_price }}" data-booking_sale_price="{{ $room->booking_sale_price }}" data-is_featured="{{ $room->is_featured }}" data-qty="{{ $room->qty }}" data-status="{{ $room->booking_status }}" class="btn btn-info btn-xs"> <i class="icon-note" id="edit_icon"></i>
                                                                </button>

                                                                <button class="btn btn-danger btn-xs"  type="button" onclick="deleteHotel({{ $room->id }})">
                                                                    <i class="fa fa-trash" id="del_icon"></i>
                                                                </button>
                                                                <form id="delete-form-{{ $room->id }}" action="{{ route('admin.room.destroy',$room->id) }}" method="POST" style="display: none;">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Hotel Modal -->
    <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.room.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div  class="form-group">
                                        <label for="hotel_name">Hotel Name</label>
                                        <select name="hotel_name" class="form-control" id="hotel_name">
                                            <option value="">-- Select Hotel Name --</option>
                                            @foreach($hotels as $hotel)
                                                <option value="{{ $hotel->id  }}">{{ $hotel->hotel_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="room_name">Room Name</label>
                                        <input type="text" id="room_name" name="room_name" class="form-control" placeholder="Room Name" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="room_size">Room Size (m²)</label>
                                        <input type="text" id="room_size" name="room_size" class="form-control" placeholder="Room Size" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sleeps">Sleeps</label>
                                        <input type="text" id="sleeps" name="sleeps" class="form-control" placeholder="Sleeps" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="booking_price">Booking Price</label>
                                        <input type="text" id="booking_price" name="booking_price" class="form-control" placeholder="Booking Price" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="booking_sale_price">Booking Sale Price</label>
                                        <input type="text" id="booking_sale_price" name="booking_sale_price" class="form-control" autocomplete="off" placeholder="Booking Sale Price">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="hFeatured">Featured</label>
                                        <select class="form-control" name="is_featured" id="hFeatured" required>
                                            <option value="0"> General </option>
                                            <option value="1"> Featured </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="booking status">Status</label>
                                        <select class="form-control" name="booking status" id="booking status">
                                            <option value="0"> Active </option>
                                            <option value="1"> Deactive </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="qty">Room Quantity</label>
                                        <input autocomplete="off" type="text" id="qty" name="qty" class="form-control" placeholder="Room Quantity" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="facilities">Room Facilities</label>
                                        <div class="row">
                                            @foreach($facilities as $facilite)
                                                <div class="col-md-4">
                                                    <label><input type="checkbox" name="facilities[]" value="{{ $facilite->id }}" > {{ $facilite->name }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="height: 300px;">
                                                <div class="wrapper">
                                                    <div class="drop">
                                                        <div class="cont">
                                                            <i class="fa fa-cloud-upload"></i>
                                                            <div class="tit">
                                                                Drag & Drop
                                                            </div>
                                                            <div class="desc">
                                                                your files to Assets, or
                                                            </div>
                                                            <div class="browse">
                                                                click here to browse
                                                            </div>
                                                        </div>
                                                        <output id="list2"></output><input id="filesEdit" multiple="true" name="files[]" type="file" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Employee Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Division</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.room.update','id') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div  class="form-group">
                                        <label for="hId">Hotel Name</label>
                                        <select name="hotel_name" class="form-control" id="hId">
                                            <option value="">-- Select Hotel Name --</option>
                                            @foreach($hotels as $hotel)
                                                <option value="{{ $hotel->id  }}">{{ $hotel->hotel_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rName">Room Name</label>
                                        <input type="text" id="rName" name="room_name" class="form-control" placeholder="Room Name" autocomplete="off">
                                        <input type="hidden" id="id" name="room_id" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rSize">Room Size (m²)</label>
                                        <input type="text" id="rSize" name="room_size" class="form-control" placeholder="Room Size" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rsleep">Sleeps</label>
                                        <input type="text" id="rsleep" name="sleeps" class="form-control" placeholder="Sleeps" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rBooking">Booking Price</label>
                                        <input type="text" id="rBooking" name="booking_price" class="form-control" placeholder="Booking Price" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rSaleBooking">Booking Sale Price</label>
                                        <input type="text" id="rSaleBooking" name="booking_sale_price" class="form-control" placeholder="Booking Sale Price" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rFeatured">Featured</label>
                                        <select class="form-control" name="is_featured" id="rFeatured">
                                            <option value="0"> General </option>
                                            <option value="1"> Featured </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rStatus">Status</label>
                                        <select class="form-control" name="booking status" id="rStatus">
                                            <option value="0"> Active </option>
                                            <option value="1"> Deactive </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rQty">Room Quantity</label>
                                        <input type="text" id="rQty" name="qty" class="form-control" placeholder="Room Quantity" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="facilities">Room Facilities</label>
{{--                                    <div class="row">--}}
{{--                                        @if($facilities->count() == "0")--}}
{{--                                        @else--}}
{{--                                            @foreach($facilities as $facilite)--}}

{{--                                                <div class="col-md-4">--}}
{{--                                                    @if($facilite->id == $faci_id)--}}
{{--                                                        <label>--}}
{{--                                                            <input type="checkbox" checked name="facilities[]"  value="{{ $facilite->id }}"> {{ $facilite->name }}--}}
{{--                                                        </label>--}}
{{--                                                    @else--}}
{{--                                                        <label>--}}
{{--                                                            <input type="checkbox" name="facilities[]"  value="{{ $facilite->id }}"> {{ $facilite->name }}--}}
{{--                                                        </label>--}}
{{--                                                    @endif--}}

{{--                                                </div>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
                                        <div class="row">
                                            <div class="col-md-12" style="height: 300px;">
                                                <div class="wrapper">
                                                    <div class="drop">
                                                        <div class="cont">
                                                            <i class="fa fa-cloud-upload"></i>
                                                            <div class="tit">
                                                                Drag & Drop
                                                            </div>
                                                            <div class="desc">
                                                                your files to Assets, or
                                                            </div>
                                                            <div class="browse">
                                                                click here to browse
                                                            </div>
                                                        </div>
                                                        <output id="list"></output><input id="files" multiple="true" name="files[]" type="file" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script type="text/javascript">
        $('#Edit').on('show.bs.modal', function (event) {
            var button          = $(event.relatedTarget);
            var roomId          = button.data('id');
            var roomName        = button.data('name');
            var hotelId         = button.data('hotelname');
            var roomSize        = button.data('size');
            var roomSleep       = button.data('sleep');
            var roomBooking     = button.data('booking_price');
            var roomSaleBooking = button.data('booking_sale_price');
            var roomFeatured    = button.data('is_featured');
            var roomQty         = button.data('qty');
            var roomStatus      = button.data('status');

            var modal = $(this);

            modal.find('.modal-body #id').val(roomId);
            modal.find('.modal-body #hId').val(hotelId);
            modal.find('.modal-body #rName').val(roomName);
            modal.find('.modal-body #rSize').val(roomSize);
            modal.find('.modal-body #rsleep').val(roomSleep);
            modal.find('.modal-body #rBooking').val(roomBooking);
            modal.find('.modal-body #rSaleBooking').val(roomSaleBooking);
            modal.find('.modal-body #rFeatured').val(roomFeatured);
            modal.find('.modal-body #rQty').val(roomQty);
            modal.find('.modal-body #rStatus').val(roomStatus);



        });
        function deleteHotel(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }

    </script>
    <script type="text/javascript">
        var i75 = 0;

        $('.addRow').on('click',function(){
            addRow();
        });
        function addRow()
        {
            i75++;
            var tr='<tr>'+
                '<td><input type="text" name="rows['+i75+'][room_name]" class="form-control" required=""></td>'+
                '<td><input type="text" name="rows['+i75+'][booking_price]" class="form-control"></td>'+
                '<td><input type="text" name="rows['+i75+'][booking_sale_price]" class="form-control"></td>'+
                '<td><input type="text" name="rows['+i75+'][qty]" class="form-control"></td>'+
                '<td><textarea  name="rows['+i75+'][about]" class="form-control"></textarea></td>'+
                '<td><input type="file" name="rows['+i75+'][image]" class="form-control"></td>'+
                '<td><button type=\'button\' class=\'btn btn-danger btn-sm btn-rect\' onclick=\'javascript:remove25(this);\'>Close</button></td>'+
                '</tr>';
            $('#customtbl').append(tr);
        };

        function remove25(index){
            console.log("hello");
            $(index).parent().parent().remove();
        }
        var drop = $("input");
        drop.on('dragenter', function (e) {
            $(".drop").css({
                "border": "4px dashed #09f",
                "background": "rgba(0, 153, 255, .05)"
            });
            $(".cont").css({
                "color": "#09f"
            });
        }).on('dragleave dragend mouseout drop', function (e) {
            $(".drop").css({
                "border": "3px dashed #DADFE3",
                "background": "transparent"
            });
            $(".cont").css({
                "color": "#8E99A5"
            });
        });


        // Add  new room
        function handleFileSelect(evt) {
            var files = evt.target.files; // FileList object
            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {
                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }
                var reader = new FileReader();
                // Closure to capture the file information.
                reader.onload = (function(theFile) {
                    return function(e) {
                        // Render thumbnail.
                        var span = document.createElement('span');
                        span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
                        document.getElementById('list').insertBefore(span, null);
                    };
                })(f);
                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        }

        $('#files').change(handleFileSelect);

        // edit
        function handleFileSelectEdit(evt) {
            var files = evt.target.files; // FileList object
            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {
                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }
                var reader = new FileReader();
                // Closure to capture the file information.
                reader.onload = (function(theFile) {
                    return function(e) {
                        // Render thumbnail.
                        var span = document.createElement('span');
                        span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
                        document.getElementById('list2').insertBefore(span, null);
                    };
                })(f);
                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        }

        $('#filesEdit').change(handleFileSelectEdit);

    </script>

@endpush
