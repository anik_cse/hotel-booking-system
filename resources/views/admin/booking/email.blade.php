<h1>{{ $title }}</h1>
<p>Dear Mr/Ms. {{ $username }},</p>
<p>Thank you for choosing to stay with us at the {{ $hotel }} Hotel. We are pleased to confirm your reservation as follows:</p>
<p>Confirmation Number: {{ $booking_id }}</p>
<p>Guest Name: Mr/Ms. {{ $username }}</p>
<p>Check-in Date : {{ $check_in}}</p>
<p>Check-out Date: {{ $check_out}}</p>
<p>Room Name: {{ $room}}</p>
<p>Should you require an early check-in, please make your request as soon as possible. Rates are quoted in U.S. funds and subject to applicable state and local taxes. If you find it necessary to cancel this reservation, the {{ $hotel }} requires notification by 4:00 P.M. the day before your arrival to avoid a charge for one night's room rate.</p>
<p>Whatever we can do to make your visit extra special, call us at 800.888.8888. Or by clicking Contact Concierge here, you will be taken to our pre-arrival checklist form where we'll assist you with advance reservations for airport transfers, dining, golf tee-times and spa treatments.</p>
<p>We look forward to the pleasure of having you as our guest at the  {{ $hotel }} Hotel.</p>
<p>Sincerely,</p>
<p>Reservations Department</p>


