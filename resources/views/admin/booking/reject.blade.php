@extends('layouts.backend.app')
@section('title','Reject Booking List')
@push('css')

@endpush

@section('content')
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Reject Booking List</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('admin.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="{{ route('admin.booking.index') }}">Booking</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Reject Booking List</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-line">
                    <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tab1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-box">
                                        <div class="card-head">
                                            <header>Reject Booking List <span class="badge bg-info">{{ $rejectBooking->count() }}</span></header>

                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-6">
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.booking.create') }}" class="btn btn-primary" > Complete Booking</a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.booking.index') }}" class="btn btn-primary" > New Booking</a>

                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-6">

                                                </div>
                                            </div>
                                            <div class="table-scrollable">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example4">
                                                    <thead>
                                                    <tr class="center">
                                                        <th>#</th>
                                                        <th>Hotel Name</th>
                                                        <th>Room Name</th>
                                                        <th>Rant Price</th>
                                                        <th>CheckIn</th>
                                                        <th>CheckOut</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Phone</th>
{{--                                                        <th>Status</th>--}}
                                                        <th>Action </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($rejectBooking as $key=> $booking)
                                                            <tr class="center">
                                                                <td>{{ $key+1 }}</td>
                                                                <td>{{ $booking->hotel->hotel_name }}</td>
                                                                <td>{{ $booking->room->room_name }}</td>
                                                                <td>{{ $booking->total_amount }}</td>
                                                                <td>{{ date('d M y',strtotime($booking->booking_start)) }}</td>
                                                                <td>{{ date('d M y',strtotime($booking->booking_end ))}}</td>
                                                                <td>{{ $booking->username }}</td>
                                                                <td>{{ $booking->email }}</td>
                                                                <td>{{ $booking->phone }}</td>
{{--                                                                <td>--}}
{{--                                                                    @if($booking->booking_status == "0")--}}
{{--                                                                        <span class="btn btn-danger btn-sm">Pending</span>--}}

{{--                                                                    @else--}}
{{--                                                                        <span class="btn btn-success btn-sm">Confirmed</span>--}}
{{--                                                                    @endif--}}
{{--                                                                </td>--}}
                                                                <td>

                                                                    <button class="btn btn-danger btn-xs"  type="button" onclick="deleteHotel({{ $booking->id }})">
                                                                        <i class="fa fa-trash" id="del_icon"></i>
                                                                    </button>
                                                                    <form id="delete-form-{{ $booking->id }}" action="{{ route('admin.booking.destroy',$booking->id) }}" method="POST" style="display: none;">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                    </form>
                                                                </td>

                                                            </tr>
                                                        @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script type="text/javascript">
        $('#Edit').on('show.bs.modal', function (event) {
            var button          = $(event.relatedTarget);
            var bookingId        = button.data('id');
            var bookId           = button.data('bookingid');
            var bookingUser      = button.data('username');
            var bookingEmail     = button.data('email');
            var bookingPhone     = button.data('phone');
            var bookingHotel     = button.data('hotel');
            var bookingRoom      = button.data('room');
            var bookingRoomId    = button.data('room_id');
            var bookingCheckin   = button.data('checkin');
            var bookingCheckout  = button.data('checkout');
            var bookingAmount    = button.data('amount');

            var modal = $(this);

            modal.find('.modal-body #bId').val(bookingId);
            modal.find('.modal-body #bookId').val(bookId);
            modal.find('.modal-body #bUesr').val(bookingUser);
            modal.find('.modal-body #bEmail').val(bookingEmail);
            modal.find('.modal-body #bPhone').val(bookingPhone);
            modal.find('.modal-body #bHotel').val(bookingHotel);
            modal.find('.modal-body #bRoom').val(bookingRoom);
            modal.find('.modal-body #bRoomId').val(bookingRoomId);
            modal.find('.modal-body #bCheckin').val(bookingCheckin);
            modal.find('.modal-body #bCheckout').val(bookingCheckout);
            modal.find('.modal-body #bAmount').val(bookingAmount);




        });
        function deleteHotel(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>

@endpush
