@extends('layouts.backend.app')
@section('title','Hotel List')
@push('css')

@endpush

@section('content')
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Hotel List</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('admin.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="{{ route('admin.owner.index') }}">Owner</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Hotel List</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-line">
                    <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tab1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-box">
                                        <div class="card-head">
                                            <header>Hotel List <span class="badge bg-info">{{ $owners->count() }}</span></header>

                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-6">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNew">
                                                            Add New <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-6">

                                                </div>
                                            </div>
                                            <div class="table-scrollable">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example4">
                                                    <thead>
                                                    <tr class="center">
                                                        <th>#</th>
                                                        <th> Image </th>
                                                        <th> Name </th>
                                                        <th> Phone </th>
                                                        <th> Email </th>
                                                        <th> Status </th>
                                                        <th> Create_At </th>
                                                        <th> Action </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($owners as $key=>$owner)
                                                        <tr class="odd gradeX">

                                                            <td>{{ $key+1 }}</td>
                                                            <td class="center"><img src="{{ asset('storage/profile/'.$owner->image) }}" width="50px" height="50" alt="Employee Image"> </td>
                                                            <td class="center">{{ $owner->name }}</td>
                                                            <td class="center">+880{{ $owner->phone }}</td>
                                                            <td class="center">{{ $owner->email }}</td>
                                                            <td class="center">
                                                                @if($owner->status =="1")
                                                               <span class="btn btn-success btn-sm">Active</span>
                                                                @else
                                                                    <span class="btn btn-danger btn-sm">Inactive</span>
                                                                @endif
                                                            </td>
                                                            <td class="center">{{date('d/m/Y', strtotime($owner->created_at))}}</td>


                                                            <td class="center">

                                                                <button data-toggle="modal" data-target="#Edit" data-id="{{ $owner->id }}" data-name="{{ $owner->name }}" data-phone="{{ $owner->phone }}" data-status="{{ $owner->status }}" data-email="{{  $owner->email }}" class="btn btn-info btn-xs"> <i class="icon-note" id="edit_icon"></i></button>



                                                                <button class="btn btn-danger btn-xs"  type="button" onclick="deleteEmployee({{ $owner->id }})">
                                                                    <i class="fa fa-trash" id="del_icon"></i>
                                                                </button>
                                                                <form id="delete-form-{{ $owner->id }}" action="{{ route('admin.owner.destroy',$owner->id) }}" method="POST" style="display: none;">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Employee Modal -->
    <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Hotel Owner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.owner.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" pattern="[0-9]{11}" name="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Employee Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Hotel Owner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.owner.update','id') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" id="name" class="form-control">
                            <input type="hidden" name="id" id="id" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" pattern="[0-9]{11}" id="phone" name="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Employee email</label>
                            <input type="text" name="email" id="emailAddress" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="status">Staus</label>
                            <select class="form-control" name="status" id="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script type="text/javascript">
        $('#Edit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var ownerId = button.data('id');
            var ownerName = button.data('name');
            var ownerPhone = button.data('phone');
            var ownerEmail = button.data('email');
            var ownerStatus = button.data('status');

            var modal = $(this);

            modal.find('.modal-body #id').val(ownerId);
            modal.find('.modal-body #name').val(ownerName);
            modal.find('.modal-body #phone').val(ownerPhone);
            modal.find('.modal-body #emailAddress').val(ownerEmail);
            modal.find('.modal-body #status').val(ownerStatus);


        });
        function deleteEmployee(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
