@extends('layouts.backend.app')

@section('title','Admin-Dashboard')
@push('css')
    <!-- data tables -->
    <link href="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <!--sweetalert2 -->
    <link href="{{ asset('backend/assets/css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
    {{--<link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">--}}
@endpush

@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Dashboard</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('admin.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
            <!-- start widget -->
            <div class="state-overview">
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        <div class="overview-panel deepPink-bgcolor">
                            <div class="symbol">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                            </div>
                            <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value=" {{ $owners->count() }}">{{ $owners->count() }}</p>
                                <p>Hotel Owners</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="overview-panel orange">
                            <div class="symbol">
                                <i class="fa fa-h-square" aria-hidden="true"></i>
                            </div>
                            <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="{{ $hotels->count() }}">{{ $hotels->count() }}</p>
                                <p>Hotels</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="overview-panel deepPink-bgcolor">
                            <div class="symbol">
                                <i class="fa fa-university" aria-hidden="true"></i>
                            </div>
                            <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="{{ $rooms->count() }}">{{ $rooms->count() }}
                                <p>Rooms </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        <div class="overview-panel orange">
                            <div class="symbol">
                                <i class="fa fa-diamond" aria-hidden="true"></i>
                            </div>
                            <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="{{ $facilities->count() }}">{{ $facilities->count() }}</p>
                                <p>Facilities</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="overview-panel deepPink-bgcolor">
                            <div class="symbol">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                            </div>
                            <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="{{ $complete_booking->count() }}">{{ $complete_booking->count() }}</p>
                                <p>Complete Booking</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="overview-panel orange">
                            <div class="symbol">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
                            <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="{{ $pending_booking->count() }}">{{ $pending_booking->count() }}</p>
                                <p>New Booking</p>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

        </div>
    </div>
@endsection

@push('js')
    <!-- counterup js -->
    <script src="{{ asset('backend/assets/plugins/counterup/jquery.waypoints.min.js') }}" ></script>
    <script src="{{ asset('backend/assets/plugins/counterup/jquery.counterup.min.js') }}" ></script>

    <!-- data tables -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}" ></script>
    <script src="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}" ></script>
    <script src="{{ asset('backend/assets/js/pages/table/table_data.js') }}" ></script>
    <script src="{{ asset('https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.js') }}" ></script>


@endpush
