@extends('layouts.backend.app')
@section('title','Hotel List')
@push('css')

@endpush

@section('content')
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Hotel List</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('admin.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="{{ route('admin.hotel.index') }}">Hotel</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Hotel List</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-line">
                    <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tab1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-box">
                                        <div class="card-head">
                                            <header>Hotel List <span class="badge bg-info">{{ $hotels->count() }}</span></header>

                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-6">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNew">
                                                            Add New <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-6">

                                                </div>
                                            </div>
                                            <div class="table-scrollable">
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example4">
                                                    <thead>
                                                    <tr class="center">
                                                        <th>#</th>
                                                        <th>Image </th>
                                                        <th>Name </th>
                                                        <th>Owner Name </th>
{{--                                                        <th>About</th>--}}
                                                        <th>Featured</th>
                                                        <th>Division</th>
                                                        <th>District</th>
                                                        <th>Area</th>
                                                        <th>Create_At </th>
                                                        <th>Action </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($hotels as $key=>$hotel)
                                                        <tr class="odd gradeX">

                                                            <td>{{ $key+1 }}</td>
                                                            <td class="center"><img src="{{ asset('storage/hotel/'.$hotel->hotel_image) }}" alt="hotel image" width="50px" height="50px"></td>
                                                            <td class="center">{{ $hotel->hotel_name }}</td>
                                                            <td class="center">
                                                                @php
                                                                    $owner = DB::table('users')->where('id',$hotel->user_id)->first();
                                                                    echo $owner->name;
                                                                @endphp

                                                            </td>
{{--                                                            <td class="center">{{ str_limit($hotel->hotel_profile,'20') }}</td>--}}
                                                            <td class="center">
                                                                @if($hotel->is_featured == "1")
                                                                    <span style="color: green"><i class="fa fa-check-square" aria-hidden="true"></i>
                                                                @else
                                                                    <span style="color: red"><i class="fa fa-window-close" aria-hidden="true"></i> </span>
                                                                @endif
                                                            </td>
                                                            <td class="center">{{ $hotel->divisions }}</td>
                                                            <td class="center">{{ $hotel->zillas }}</td>
                                                            <td class="center">{{ $hotel->city }}</td>
                                                            <td class="center">{{date('d/m/Y', strtotime($hotel->created_at))}}</td>


                                                            <td class="center">

                                                                <button data-toggle="modal" data-target="#Edit" data-id="{{ $hotel->id }}" data-name="{{ $hotel->hotel_name }}" data-division="{{ $hotel->divisions }}" data-zillas="{{ $hotel->zillas }}" data-city="{{ $hotel->city }}" data-featured="{{ $hotel->is_featured }}" data-owner="{{ $hotel->user_id }}" data-about="{{ $hotel->hotel_profile }}" class="btn btn-info btn-xs"> <i class="icon-note" id="edit_icon"></i></button>



                                                                <button class="btn btn-danger btn-xs"  type="button" onclick="deleteHotel({{ $hotel->id }})">
                                                                    <i class="fa fa-trash" id="del_icon"></i>
                                                                </button>
                                                                <form id="delete-form-{{ $hotel->id }}" action="{{ route('admin.hotel.destroy',$hotel->id) }}" method="POST" style="display: none;">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Hotel Modal -->
    <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Hotel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.hotel.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="hotel_name">Hotel Name</label>
                            <input type="text" name="hotel_name" id="hotel_name" class="form-control" placeholder="Hotel Name">
                        </div>
                        <div class="form-group">
                            <label for="division">Division Name</label>
                            <input type="text" name="division" id="division" class="form-control" placeholder="Division Name">
                        </div>
                        <div class="form-group">
                            <label for="district">District Name</label>
                            <input type="text" name="district" id="district" class="form-control" placeholder="District Name">
                        </div>
                        <div class="form-group">
                            <label for="area">Area</label>
                            <input type="text" name="area" id="area" class="form-control" placeholder="Area Name (Optional)">
                        </div>

                        <div class="form-group">
                            <label for="owner">Owner Name</label>
                           <select class="form-control" name="owner" id="owner">
                               @foreach($owners as $owner)
                                   <option value="{{ $owner->id }}">{{ $owner->name }}</option>
                               @endforeach
                           </select>
                        </div>

                        <div class="form-group">
                            <label for="hFeatured">Featured</label>
                            <select class="form-control" name="is_featured" id="hFeatured">
                                <option value="0"> General </option>
                                <option value="1"> Featured </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="about">About</label>
                            <textarea name="about" id="about" cols="10" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" name="image" class="form-control">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Employee Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Hotel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.hotel.update','id') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="hName">Hotel Name</label>
                            <input type="text" name="hotel_name" id="hName" class="form-control" placeholder="Hotel Name">
                            <input type="hidden" name="id" id="id" class="form-control" placeholder="Hotel Name">
                        </div>
                        <div class="form-group">
                            <label for="hDivision">Division Name</label>
                            <input type="text" name="division" id="hDivision" class="form-control" placeholder="Division Name">
                        </div>
                        <div class="form-group">
                            <label for="hDistrict">District Name</label>
                            <input type="text" name="district" id="hDistrict" class="form-control" placeholder="District Name">
                        </div>
                        <div class="form-group">
                            <label for="hCity">Area</label>
                            <input type="text" name="area" id="hCity" class="form-control" placeholder="Area Name (Optional)">
                        </div>
                        <div class="form-group">
                            <label for="hOwner">Owner Name</label>
                            <select class="form-control" name="owner" id="hOwner">
                                @foreach($owners as $owner)
                                    <option value="{{ $owner->id }}">{{ $owner->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="hFeatured">Featured</label>
                            <select class="form-control" name="is_featured" id="hFeatured">
                               <option value="0"> General </option>
                               <option value="1"> Featured </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="hProfile">About</label>
                            <textarea name="about" id="hProfile" cols="10" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" name="image" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script type="text/javascript">
        $('#Edit').on('show.bs.modal', function (event) {
            var button          = $(event.relatedTarget);
            var hotelId         = button.data('id');
            var hotelName       = button.data('name');
            var hotelDivision   = button.data('division');
            var hotelDistrict   = button.data('zillas');
            var hotelCity       = button.data('city');
            var hotelOwner      = button.data('owner');
            var hotelProfile    = button.data('about');
            var hotelFeatured    = button.data('featured');

            var modal = $(this);

            modal.find('.modal-body #id').val(hotelId);
            modal.find('.modal-body #hName').val(hotelName);
            modal.find('.modal-body #hDivision').val(hotelDivision);
            modal.find('.modal-body #hDistrict').val(hotelDistrict);
            modal.find('.modal-body #hCity').val(hotelCity);
            modal.find('.modal-body #hOwner').val(hotelOwner);
            modal.find('.modal-body #hProfile').val(hotelProfile);
            modal.find('.modal-body #hFeatured').val(hotelFeatured);



        });
        function deleteHotel(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }

        {{--$(function () {--}}
            {{--var division = $('select[name="division_id"]'),--}}
                {{--district = $('select[name="district_id"]');--}}

            {{--division.change(function () {--}}
                {{--var id = $(this).val();--}}
                {{--if(id){--}}
                    {{--district.attr('disabled','disabled');--}}
                    {{--$.get('{{ url('admin/district?division_id=') }}'+id)--}}
                        {{--.success(function (data) {--}}
                            {{--var s='<option value="">-- Select --</option>';--}}
                            {{--data.forEach(function (row) {--}}
                                {{--s +='<option value="'+row.id+'">'+row.name+'</option>'--}}
                            {{--});--}}
                            {{--district.removeAttr('disabled');--}}
                            {{--district.html(s);--}}
                        {{--})--}}
                {{--}--}}
            {{--})--}}
        {{--})--}}
    </script>

@endpush
