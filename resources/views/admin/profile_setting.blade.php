@extends('layouts.backend.app')

@section('title','Profile - Setting')
@push('css')

@endpush

@section('content')
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Profile Setting</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ route('admin.dashboard') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>

                    </li>
                    <li class="active">Profile Setting</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-8 offset-2">
                <div class="panel tab-border card-box">
                    <header class="panel-heading panel-heading-gray custom-tab">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="#about-2" data-toggle="tab" class="active show">
                                    <i class="fa fa-user"></i> Profile
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#contact-2" data-toggle="tab" class="">
                                    <i class="fa fa-cogs"></i> Change Password
                                </a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="about-2">
                                <form method="POST" action="{{ route('admin.profile.update',$profile->id) }}" enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 control-label">Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="name" value="{{ $profile->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 control-label">Phone</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" maxlength="10" name="phone" value="{{ $profile->phone }}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" name="email" value="{{ $profile->email }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 control-label">Profile Image</label>
                                        <div class="col-sm-8">
                                            <input type="file" class="form-control" name="image" >
                                        </div>
                                    </div>
                                    {{--<div class="form-group row">--}}
                                        {{--<label for="body" class="col-sm-3 control-label">About</label>--}}
                                        {{--<div class="col-sm-8">--}}
                                            {{--<textarea name="about"  cols="30" rows="6" class="tinymce">--}}
                                                {{--{{ $profile->about }}--}}
					                        {{--</textarea>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-group">
                                        <div class="offset-md-3 col-md-9">
                                            <button type="submit" class="btn btn-info m-r-20">Update</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="contact-2">
                                <form method="POST" action="{{ route('admin.password.update') }}" class="form-horizontal">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group row">
                                        <label for="old password" class="col-sm-3 control-label">Old Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" name="old_password" class="form-control" id="old password" placeholder="Enter your old password" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="new password" class="col-sm-3 control-label">New Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" name="password" class="form-control" id="new password" placeholder="Enter your new password" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="old password" class="col-sm-3 control-label">Confirm Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" name="password_confirmation" class="form-control" id="Confirmed password" placeholder="Enter your new password again" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="offset-md-3 col-md-9">
                                            <button type="submit" class="btn btn-info m-r-20">Update</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <!-- tinymce -->
    <script src="{{ asset('backend/assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/tinymce/init-tinymce.js') }}"></script>
@endpush
