@extends('layouts.app')
@section('title','All Hotels')
@push('css')

@endpush

@section('content')

    <!-- Content -->
    <div class="page-content">
        <!-- Main Slider -->
        @include('layouts.fontend.slider')
        <!-- Main Slider -->

        <!-- About Us -->
        <div class="section-full bg-white content-inner dlab-about-1 promotions" id="about-us">
            <div class="container">
                <div class="section-head text-center">
                    <h2 class="text-uppercase m-b0">POPULAR HOTELS IN BANGLADESH</h2>
                    <p class="font-18">BEST TRAVEL PACKAGES AVAILABLE</p>
                </div>

                <div class="row" id="masonry">
                    @foreach($hotels as $hotel)
                        <div class="col-lg-4 col-md-6 col-sm-6 m-b30 card-container">
                            <div class="dlab-box">
                                <div class="dlab-media"> <a href="{{ route('hotel-details',[$hotel->id,$hotel->slug]) }}">
                                        <img src="{{ asset('storage/hotel/'.$hotel->hotel_image) }}" alt=""></a>
                                    <div class="tr-price">
                                        <span>
                                            {{ $hotel->divisions }}
                                        </span>
                                    </div>
                                </div>
                                <div class="dlab-info p-a20 border-1 text-center">
                                    <h4 class="dlab-title m-t0"><a href="{{ route('hotel-details',[$hotel->id,$hotel->slug]) }}">{{ $hotel->hotel_name }}</a></h4>
                                    <p>{{ Str::limit($hotel->hotel_profile,100) }}</p>

                                    <div class="tr-btn-info">
                                        <a href="#"  class="site-button radius-no"><i class="fa fa-location-arrow" aria-hidden="true"></i> Booking Now</a>
                                        <a href="{{ route('hotel-details',[$hotel->id,$hotel->slug]) }}" class="site-button bg-primary-dark radius-no"><i class="fa fa-info-circle" aria-hidden="true"></i>  Details </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="paginate">
                {{ $hotels->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')


@endpush
