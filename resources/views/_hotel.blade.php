@extends('layouts.app')
@section('title',$hotelDetails->hotel_name)
@push('css')

@endpush

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="dlab-bnr-inr overlay-black-middle" style="background-image:url(images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dlab-bnr-inr-entry">
                    <h1 class="text-white">Hotal Booking</h1>
                    <!-- Breadcrumb row -->
                    <div class="breadcrumb-row">
                        <ul class="list-inline">
                            <li><a href="#">Home</a></li>
                            <li>Hotal Details</li>
                        </ul>
                    </div>
                    <!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
        <div class="content-block">
            <div class="section-full content-inner">
                <div class="container">
                    <div class="row m-b30">
                        <div class="col-lg-8">
                            <div class="d-flex info-bx m-b30">
                                <div class="tour-title">
                                    <h2>{{ $hotelDetails->hotel_name }}</h2>
                                    <p><i class="fa fa-map-marker m-r5"></i>Yeshwanthpur, Bangalore <a href="#" class="text-primary">View on Map</a></p>
                                    <p><span class="site-button button-sm button-gray">Hotel</span> <span class="site-button button-sm">Bar</span> Tour</p>
                                </div>
                                <div class="tour-price ml-auto">
                                    <span>Per Room Per Night</span>
                                    <h2 class="price">Rs.1,07,990</h2>
                                    <h4 class="actual-price">Rs.1,23,990</h4>
                                </div>
                            </div>
                            <div class="product-gallery on-show-slider">
                                <div id="sync1" class="owl-carousel owl-theme owl-btn-center-lr m-b5 owl-btn-1 primary">
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic1.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic2.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic3.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic4.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic5.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic6.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic7.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic8.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic9.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-box">
                                            <div class="dlab-thum-bx">
                                                <img src="{{ asset('storage/hotel/pic10.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="sync2" class="owl-carousel owl-theme owl-none">
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb3.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb4.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb5.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb6.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb7.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb8.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb9.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="dlab-media">
                                            <img src="{{ asset('storage/hotel/pic-thumb10.jpg') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tour-days">
                                <h2 class="m-b10">About Hotel</h2>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <ul class="list-hand-point primary">
                                            <li>Closeness to ISRO (1.6 km) and BEL (2.4 km)</li>
                                            <li>Cozy rooms with modern interiors</li>
                                            <li>In-house restaurant serving tasty dishes</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <h5>Where we are Located</h5>
                                        <ul class="list-hand-point primary">
                                            <li>The FabHotel RMS Comforts is situated on 5th Main in the Mathikere Extension area</li>
                                            <li>Yeshwantpur Junction Railway Station is 1.8 km, while Krantivira Sangolli Rayanna Railway Station is 7.3 km from the hotel</li>
                                            <li>Sandal Soap Factory Metro Station is 2.6 km and Kempegowda International Airport is a 40-minute drive (30.5 km)</li>
                                            <li>Some of the prominent landmarks near the hotel include BBMP Office (700 m), Ramaiah Institute of Technology (750 m), Indian Institute of Science (950 m), BEL-THALES Systems Limited (1.5 km), ISRO (1.6 km), RTO Office Yeshwanthpura (1.8 km), Antrix Corporation Limited (1.9 km), Bharat Electronics Limited (2.1 km) and Professional Tax Office (2.5 km)</li>
                                            <li>Sandal Soap Factory Metro Station is 2.6 km and Kempegowda International Airport is a 40-minute drive (30.5 km)</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <h5>Where to Eat</h5>
                                        <ul class="list-hand-point primary">
                                            <li>The hotel has a restaurant that treats you with a wide range of dishes across multiple cuisines</li>
                                            <li>Sri Krishna Bhavan (53 m), Shree Sagar (63 m), Delight (72 m), Reddy Mess (140 m), Star Biryani Center (290 m) and Indira Canteen (300 m) are among many dining options around the hotel</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="sticky-top">
                                <form class="hotel-booking">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6 col-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input name="dzName" required="" class="form-control" placeholder="" type="date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6 col-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input name="dzName" required="" class="form-control" placeholder="" type="date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xl-4 col-sm-6 col-6">
                                            <div class="form-group">
                                                <div class="quantity btn-quantity">
                                                    <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                                </div>
                                                <span class="font-12">Rooms</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xl-4 col-sm-6 col-6">
                                            <div class="form-group">
                                                <div class="quantity btn-quantity">
                                                    <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                                </div>
                                                <span class="font-12">Adults</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xl-4 col-sm-6 col-6">
                                            <div class="form-group">
                                                <div class="quantity btn-quantity">
                                                    <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                                </div>
                                                <span class="font-12">Children</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xl-12 col-sm-6 col-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <select class="selectpicker" multiple data-actions-box="true">
                                                        <option>Deluxe Twin Bed Room</option>
                                                        <option>Breakfast and Dinner</option>
                                                        <option>Deluxe Twin (Smoking)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12">
                                            <div class="booking-total">
                                                <h3 class="d-flex">Rs.40000 <span>Sub Total1 room for 1 night</span></h3>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12">
                                            <button type="submit" class="site-button btn-block">Book Now</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="m-t30">
                                    <img src="images/add/add-bnr.jpg" class="d-md-none d-xl-block d-lg-block" alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-b30">
                        <div class="col-md-12 col-lg-12">
                            <div class="day-details-bx">
                                <div class="row">
                                    <div class="col-md-12 col-lg-4">
                                        <h4 class="m-b5">Arrival in London </h4>
                                        <div class="m-b10">
                                            <ul class="rating-star">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul class="list-check primary">
                                                    <li>Fits 2</li>
                                                    <li>Study table</li>
                                                    <li>Bathroom</li>
                                                    <li>Fits 2</li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6">
                                                <ul class="list-check primary">
                                                    <li>Bathroom</li>
                                                    <li>Study table</li>
                                                    <li>LCD TV</li>
                                                    <li>Study table</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <ul class="tour-tag">
                                            <li><a href="#">Breakfast</a></li>
                                            <li><a href="#">Dinner</a></li>
                                            </li>
                                    </div>
                                    <div class="col-md-7 col-lg-4">
                                        <div class="info-bx ">
                                            <p>The hotel has a restaurant that treats you with a wide range of dishes across multiple cuisines</p>
                                            <div class="tour-price">
                                                <span>Per Room Per Night</span>
                                                <h2 class="price">Rs.1,07,990</h2>
                                                <h4 class="actual-price">Rs.1,23,990</h4>
                                            </div>
                                            <div class="m-t20 m-b30">
                                                <a class="site-button red" href="#">Remove</a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-5 col-lg-4">
                                        <img src="images/our-work/pic1.jpg" class="radius-sm" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Why Chose Us -->
                <div class="modal fade submit-query" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Check Price & Availability</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Customer City</label>
                                            <div class="input-group">
                                                <select class="form-control">
                                                    <option>Select City</option>
                                                    <option>Kochi</option>
                                                    <option>Mumbai</option>
                                                    <option>New Delhi</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Customer State</label>
                                            <div class="input-group">
                                                <select class="form-control">
                                                    <option>Select State</option>
                                                    <option>Kochi</option>
                                                    <option>Mumbai</option>
                                                    <option>New Delhi</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Departure Date Selected</label>
                                            <div class="input-group">
                                                <input name="dzName" required="" class="form-control" type="date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="quantity btn-quantity">
                                            <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                            <span class="font-12">Adult (12yrs +)</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="quantity btn-quantity">
                                            <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                            <span class="font-12">Child (2-12yrs)</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="quantity btn-quantity">
                                            <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                            <span class="font-12">Infant (0-2yrs)</span>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="modal-footer d-flex justify-content-between">
                                <button type="submit" class="site-button-secondry" data-dismiss="modal">Close</button>
                                <button type="submit" class="site-button">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade submit-query" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Get the Best Holiday Planned by Experts!</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h5 class="text-center">Enter your contact details and we will plan the best holiday suiting all your requirements.</h5>
                                <form class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="dzName" required="" class="form-control" placeholder="Your Name" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="dzName" required="" class="form-control" placeholder="Your Email Address" type="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="dzName" required="" class="form-control" placeholder="Mobile No" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="dzName" required="" class="form-control" placeholder="Your City" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="dzName" required="" class="form-control" type="date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="quantity btn-quantity">
                                            <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                            <span class="font-12">Adult (12yrs +)</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="quantity btn-quantity">
                                            <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                            <span class="font-12">Child (2-12yrs)</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="quantity btn-quantity">
                                            <input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
                                            <span class="font-12">Infant (0-2yrs)</span>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="modal-footer d-flex justify-content-between">
                                <button type="submit" class="site-button-secondry" data-dismiss="modal">Close</button>
                                <button type="submit" class="site-button">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->
@endsection

@push('js')
    <script>
        $(document).ready(function() {

            var sync1 = $("#sync1");
            var sync2 = $("#sync2");
            var slidesPerPage = 8; //globaly define number of elements per page
            var syncedSecondary = true;

            sync1.owlCarousel({
                items : 1,
                slideSpeed : 2000,
                nav: true,
                autoplay: false,
                dots: false,
                loop: true,
                responsiveRefreshRate : 200,
                navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
            }).on('changed.owl.carousel', syncPosition);

            sync2.on('initialized.owl.carousel', function () {
                sync2.find(".owl-item").eq(0).addClass("current");
            }).owlCarousel({
                items : slidesPerPage,
                dots: false,
                nav: false,
                margin:5,
                smartSpeed: 200,
                slideSpeed : 500,
                slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                responsiveRefreshRate : 100
            }).on('changed.owl.carousel', syncPosition2);

            function syncPosition(el) {
                //if you set loop to false, you have to restore this next line
                //var current = el.item.index;

                //if you disable loop you have to comment this block
                var count = el.item.count-1;
                var current = Math.round(el.item.index - (el.item.count/2) - .5);

                if(current < 0) {
                    current = count;
                }
                if(current > count) {
                    current = 0;
                }

                //end block

                sync2
                    .find(".owl-item")
                    .removeClass("current")
                    .eq(current)
                    .addClass("current");
                var onscreen = sync2.find('.owl-item.active').length - 1;
                var start = sync2.find('.owl-item.active').first().index();
                var end = sync2.find('.owl-item.active').last().index();

                if (current > end) {
                    sync2.data('owl.carousel').to(current, 100, true);
                }
                if (current < start) {
                    sync2.data('owl.carousel').to(current - onscreen, 100, true);
                }
            }

            function syncPosition2(el) {
                if(syncedSecondary) {
                    var number = el.item.index;
                    sync1.data('owl.carousel').to(number, 100, true);
                }
            }

            sync2.on("click", ".owl-item", function(e){
                e.preventDefault();
                var number = $(this).index();
                //sync1.data('owl.carousel').to(number, 300, true);

                sync1.data('owl.carousel').to(number, 300, true);

            });
        });
    </script>

@endpush
