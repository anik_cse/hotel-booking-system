@extends('layouts.app')
@section('title','Home Page')
@push('css')
    <style>
        .paginate{
            float: right;
        }
        .gj-textbox-md {
            /* border: none; */
            /* border-bottom: 1px solid rgba(0,0,0,.42); */
            display: block;
            font-family: Helvetica,Arial,sans-serif;
            font-size: 16px;
            line-height: 16px;
            /* padding: 4px 0; */
            margin: 0;
            width: 100%;
            /* background: 0 0; */
            text-align: left;
            color: rgba(0,0,0,.87);
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
@endpush

@section('content')

    <!-- Content -->
    <div class="page-content">
        <!-- Main Slider -->
{{--        @include('layouts.fontend.slider')--}}
        <div class="section-full book-form overlay-black-dark bg-img-fix p-t30 p-b10 mid" style="background-image:url( {{ asset('storage/site/bg5.jpg') }}); height: 400px;">
            <div class="container">
                <form class="row" style="margin-top: 108px;" action="{{ route('search') }}" method="POST">
                    @csrf
                    <div class="col-md-4 col-sm-6 col-6 col-lg-3 form-group">
                        <label>Hotel/Area</label>
                        <input class="typeahead form-control" placeholder="Hotel/Area" type="text" id="search" name="keyword">
                        <script type="text/javascript">
                            var path = "{{ route('autocomplete') }}";
                            $('input.typeahead').typeahead({
                                source:  function (query, process) {
                                    return $.get(path, { query: query }, function (data) {
                                        return process(data);
                                    });
                                }
                            });
                        </script>
                    </div>

                    <div class="col-md-4 col-sm-6 col-6 col-lg-3 form-group">
                        <label>Check-in</label>
                        <input type="text" id="check_in" name="check_in" value="{{ date('Y-m-d') }}" class="form-control" autocomplete="off"/>
                        <script>
                            $('#check_in').datepicker({
                                format: 'yyyy-mm-dd',
                                autoclose: false,
                            });
                        </script>
                    </div>
                    <div class="col-md-4 col-sm-6 col-6 col-lg-3 form-group">
                        <label for="check_out">Check-out</label>
                        <input type="text" id="check_out" name="check_out" value="{{ date('Y-m-d') }}" class="form-control"  autocomplete="off"/>
                        <script>
                            $('#check_out').datepicker({
                                format: 'yyyy-mm-dd',
                                autoclose: false,
                            });
                        </script>
                    </div>
                    <div class="col-md-4 col-sm-6 col-6 col-lg-3 form-group">
                        <label>Find</label>
                        <button type="submit"  class="site-button btn-block">SEARCH</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- Main Slider -->
        <div class="section-full bg-white content-inner dlab-about-1 promotions" id="about-us">
            <div class="container">
                <div class="section-head text-center">
                    <h2 class="text-uppercase m-b0">POPULAR HOTELS IN BANGLADESH</h2>
                    <p class="font-18">BEST TRAVEL PACKAGES AVAILABLE</p>
                </div>

                <div class="row" id="masonry">
                    @foreach($hotels as $hotel)
                        <div class="col-lg-4 col-md-6 col-sm-6 m-b30 card-container">
                            <div class="dlab-box">
                                <div class="dlab-media"> <a href="{{ route('hotel-details',[$hotel->id,$hotel->slug]) }}">
                                        <img src="{{ asset('storage/hotel/'.$hotel->hotel_image) }}" alt=""></a>
                                    <div class="tr-price">
                                        <span>
                                            {{ $hotel->divisions }}
                                        </span>
                                    </div>
                                </div>
                                <div class="dlab-info p-a20 border-1 text-center">
                                    <h4 class="dlab-title m-t0"><a href="{{ route('hotel-details',[$hotel->id,$hotel->slug]) }}">{{ $hotel->hotel_name }}</a></h4>
                                    <p>{{ Str::limit($hotel->hotel_profile,100) }}</p>

                                    <div class="tr-btn-info">
                                        <a href="#"  class="site-button radius-no"><i class="fa fa-location-arrow" aria-hidden="true"></i> Booking Now</a>
                                        <a href="{{ route('hotel-details',[$hotel->id,$hotel->slug]) }}" class="site-button bg-primary-dark radius-no"><i class="fa fa-info-circle" aria-hidden="true"></i>  Details </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="paginate">
                {{ $hotels->links() }}
                </div>
            </div>
        </div>

        <div class="section-full bg-white content-inner dlab-about-1 promotions">
            <div class="container">
                <div class="section-head text-black text-center">
                    <h2 class="text-uppercase m-b0">FEATURE ROOMS</h2>
                    <p class="font-18">BEST TRAVEL PACKAGES </p>
                </div>
                <div class="row packages">
                    @foreach($rooms as $room)
                        <div class="col-lg-6 col-xl-3 col-sm-6 col-md-6 m-b30">
                        <div class="dlab-box">
                            <div class="dlab-media">
                                @php
                                    $room_image = DB::table('room_image')->where('room_id',$room->id)->first();

                                @endphp
                                <a href="{{ route('room',[$room->id,$room->room_name]) }}"><img src="{{ asset('storage/room/'.$room_image->image) }}" alt=""></a>
                            </div>
                            <div class="dlab-info p-a15 border-1">
                                <h4 class="dlab-title m-t0"><a href="{{ route('room',[$room->id,$room->room_name]) }}">{{ $room->room_name }}</a></h4>
                                <span class="location">
                                    @php
                                    $hotel = DB::table('hotels')->where('id',$room->hotel_id)->first();

                                    @endphp
                                    <i class="fa fa-home" aria-hidden="true"></i> {{ $hotel->hotel_name }}
                                    <br/>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i> {{ $hotel->zillas }},{{ $hotel->divisions }}
                                </span>
                                <div class="package-content">
                                    <ul class="package-meta">
                                        <li> <span class="fa fa-user"></span> People: {{ $room->sleep }} </li>
                                        <li><span class="fa fa-home"></span> Total Room: {{ $room->qty }} </li>

                                    </ul>
                                    <div class="clearfix">
                                        @if($room->booking_sale_price == NULL)
                                            <span class="package-price pull-left text-primary">৳.{{ $room->booking_price }}</span>
                                        @else
                                            <span class=" pull-left text-primary">৳.{{ $room->booking_sale_price }}</span> &nbsp;  ৳.<del>{{ $room->booking_price }}</del>
                                        @endif
                                        <a href="{{ route('room',[$room->id,$room->room_name]) }}" class="site-button pull-right" ><i class="fa fa-location-arrow" aria-hidden="true"></i> Booking Now</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="paginate">
                    {{ $rooms->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

@endpush
