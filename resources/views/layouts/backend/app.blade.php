<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Hotel') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="{{ asset('backend/assets/fonts/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/fonts/material-design-icons/material-icon.css') }}" rel="stylesheet" type="text/css" />
    <!--bootstrap -->
    <link href="{{ asset('backend/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/material/material.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/assets/css/material_style.css') }}">
    <!-- inbox style -->
    <link href="{{ asset('backend/assets/css/pages/inbox.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme Styles -->
    <link href="{{ asset('backend/assets/css/theme/light/theme_style.css') }}" rel="stylesheet" id="rt_style_components" type="text/css" />
    <link href="{{ asset('backend/assets/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/theme/light/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/theme/light/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <!-- toastr-->
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <!-- sweetalert2 -->
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <!-- data tables -->
    <link href="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>

    @stack('css')

</head>
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    @include('layouts.backend.partial.header')
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->

        @include('layouts.backend.partial.sidebar')
        <!-- end sidebar menu -->

        <div class="page-content-wrapper">
            <!-- start page content -->
            @yield('content')
            <!-- end page content -->
        </div>
    </div>
</div>


<!-- start js include path -->
<script src="{{ asset('backend/assets/plugins/jquery/jquery.min.js') }}" ></script>
<script src="{{ asset('backend/assets/plugins/popper/popper.js') }}" ></script>
<script src="{{ asset('backend/assets/plugins/jquery-blockui/jquery.blockui.min.js') }}" ></script>
<script src="{{ asset('backend/assets/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<!-- bootstrap -->
<script src="{{ asset('backend/assets/plugins/bootstrap/js/bootstrap.min.js') }}" ></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" ></script>
<script src="{{ asset('backend/assets/plugins/sparkline/jquery.sparkline.js') }}" ></script>
<script src="{{ asset('backend/assets/js/pages/sparkline/sparkline-data.js') }}" ></script>
<!-- Common js-->
<script src="{{ asset('backend/assets/js/app.js') }}" ></script>
<script src="{{ asset('backend/assets/js/layout.js') }}" ></script>
<script src="{{ asset('backend/assets/js/theme-color.js') }}" ></script>
<!-- material -->
<script src="{{ asset('backend/assets/plugins/material/material.min.js') }}"></script>
<!--toastr.min.js -->
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{{--<script src="{{ asset('backend/assets/js/toastr.min.js') }}"></script>--}}
<!-- Sweetalert2 -->
<script src="{{ asset('https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.js') }}" ></script>
<!-- data tables -->
<script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}" ></script>
<script src="{{ asset('backend/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}" ></script>
<script src="{{ asset('backend/assets/js/pages/table/table_data.js') }}" ></script>

{!! Toastr::message() !!}
<script>
    @if($errors->any())
    @foreach($errors->all() as $error)
    toastr.error('{{ $error }}','Error',{
        closeButton:true,
        progressBar: true,
    });
    @endforeach
    @endif
</script>
@stack('js')
</body>
</html>
