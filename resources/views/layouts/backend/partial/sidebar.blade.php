<!-- start sidebar menu -->
<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll" class="left-sidemenu">
            <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>
                <li class="sidebar-user-panel">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src=" {{ asset('storage/profile/'.Auth::user()->image) }}" class="img-circle user-img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p> {{ Auth::user()->name }}</p>
                            <a href="#"><i class="fa fa-circle user-online"></i><span class="txtOnline"> Online</span></a>
                        </div>
                    </div>
                </li>
                @if(Request::is('admin*'))
                    <li class="nav-item {{ Request::is('admin/dashboard') ? 'active':'' }}">
                        <a href="{{ route('admin.dashboard') }}" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/owner*') ? 'active':'' }}">
                        <a href="{{ route('admin.owner.index') }}" class="nav-link nav-toggle"> <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                            <span class="title">Hotel Owners</span>
                        </a>
                    </li>
{{--                    <li class="nav-item {{ Request::is('admin/division*') ? 'active':'' }}">--}}
{{--                        <a href="{{ route('admin.division.index') }}" class="nav-link nav-toggle"> <i class="fa fa-bars" aria-hidden="true"></i>--}}
{{--                            <span class="title">Divisions</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item {{ Request::is('admin/zilla*') ? 'active':'' }}">--}}
{{--                        <a href="{{ route('admin.zilla.index') }}" class="nav-link nav-toggle"> <i class="fa fa-sitemap" aria-hidden="true"></i>--}}
{{--                            <span class="title">Zilla</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="nav-item {{ Request::is('admin/hotel*') ? 'active':'' }}">
                        <a href="{{ route('admin.hotel.index') }}" class="nav-link nav-toggle"> <i class="fa fa-h-square" aria-hidden="true"></i>
                            <span class="title">Hotels</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/room*') ? 'active':'' }}">
                        <a href="{{ route('admin.room.index') }}" class="nav-link nav-toggle"> <i class="fa fa-university" aria-hidden="true"></i>
                            <span class="title">Rooms</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/facilities*') ? 'active':'' }}">
                        <a href="{{ route('admin.facilities.index') }}" class="nav-link nav-toggle"> <i class="fa fa-diamond" aria-hidden="true"></i>
                            <span class="title">Facilities</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/booking*') ? 'active':'' }}">
                        <a href="{{ route('admin.booking.index') }}" class="nav-link nav-toggle"> <i class="fa fa-calendar" aria-hidden="true"></i>
                            <span class="title">Booking</span>
                        </a>
                    </li>

                    @elseif(Request::is('owner*'))
                        <li class="nav-item ">
                            <a href="{{ route('home') }}" class="nav-link nav-toggle"> <i class="material-icons">home</i>
                                <span class="title">Back to Home</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::is('owner/dashboard') ? 'active':'' }}">
                            <a href="{{ route('owner.dashboard') }}" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                    <li class="nav-item {{ Request::is('owner/room*') ? 'active':'' }}">
                        <a href="{{ route('owner.room.index') }}" class="nav-link nav-toggle"> <i class="fa fa-university" aria-hidden="true"></i>
                            <span class="title">Rooms</span>
                        </a>
                    </li>

                    @else
                        <li class="nav-item ">
                            <a href="{{ route('home') }}" class="nav-link nav-toggle"> <i class="material-icons">home</i>
                                <span class="title">Back to Home</span>
                            </a>
                        </li>

                    @endif
            </ul>
        </div>
    </div>
</div>
<!-- end sidebar menu -->

