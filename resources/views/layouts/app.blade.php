<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />
        <meta name="description" content="HBS : Hotel Booking System" />
        <meta property="og:title" content="HBS : Hotel Booking System" />
        <meta property="og:description" content="HBS : Hotel Booking System" />
        <meta property="og:image" content="social-image.png" />
        <meta name="format-detection" content="telephone=no">

{{--        <!-- FAVICONS ICON -->--}}
{{--        <link rel="icon" href="images/favicon.ico" type="image/x-icon" />--}}
{{--        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />--}}
        <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
        <!-- MOBILE SPECIFIC -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- STYLESHEETS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('fontend/assets/css/plugins.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('fontend/assets/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('fontend/assets/css/templete.css') }}">
        <link rel="stylesheet" class="skin"  type="text/css" href="{{ asset('fontend/assets/css/skin/skin-1.css') }}">
        <link rel="stylesheet" href="{{ asset('fontend/assets/plugins/datepicker/css/bootstrap-datetimepicker.min.css') }}"/>
    @stack('css')
    </head>

<body id="bg">
<!--<div id="loading-area"></div>-->
<div class="page-wraper">
    <!-- header -->
    @include('layouts.fontend.header')
    <!-- header END -->
    @yield('content')
    <!-- Footer -->
    @include('layouts.fontend.footer')
    <!-- Footer END-->
    <!-- scroll top button -->
    <button class="scroltop fa fa-plane " ></button>

</div>
<!-- JAVASCRIPT FILES ========================================= -->
<script src="{{ asset('fontend/assets/js/jquery.min.js') }}"></script><!-- WOW JS -->
<script src="{{ asset('fontend/assets/plugins/wow/wow.js') }}"></script><!-- WOW JS -->
<script src="{{ asset('fontend/assets/plugins/bootstrap/js/popper.min.js') }}"></script><!-- BOOTSTRAP.MIN JS -->
<script src="{{ asset('fontend/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script><!-- BOOTSTRAP.MIN JS -->
<script src="{{ asset('fontend/assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script><!-- FORM JS -->
<script src="{{ asset('fontend/assets/plugins/counter/counterup.min.js') }}"></script><!-- COUNTERUP JS -->
<script src="{{ asset('fontend/assets/plugins/imagesloaded/imagesloaded.js') }}"></script><!-- IMAGESLOADED -->
<script src="{{ asset('fontend/assets/plugins/owl-carousel/owl.carousel.js') }}"></script><!-- OWL SLIDER -->
<script src="{{ asset('fontend/assets/plugins/paroller/skrollr.min.js') }}"></script><!-- PAROLLER -->

@stack('js')
</body>
</html>
