<!-- header -->
<header class="site-header mo-left header">
    <!-- main header -->
    <div class="sticky-header navbar-expand-lg">
        <div class="main-bar clearfix onepage">
            <div class="container clearfix">
                <!-- website logo -->
                <div class="logo-header mostion">
                    <a href="{{ route('home') }}"><img src="{{ asset('storage/site/logo.png') }}" alt=""></a>
                </div>
                <!-- nav toggle button -->
                <button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <!-- extra nav -->
                <div class="extra-nav">
                    <div class="extra-cell">
                        <button id="quik-search-btn" type="button" class="site-button-link"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <!-- Quik search -->
                <div class="dlab-quik-search bg-primary search-style-1">
                    <form action="#">
                        <input name="search" value="" type="text" class="form-control" placeholder="Type to search">
                        <span id="quik-search-remove"><i class="ti-close"></i></span>
                    </form>
                </div>
                <!-- main nav -->
                <div class="header-nav navbar-collapse collapse navbar myNavbar justify-content-end" id="navbarNavDropdown">
                    <ul class="nav navbar-nav">
                        <li class="{{ Request::is('/') ? 'active' :'' }} "><a href="{{route('home')}}">Home</a></li>
                        <li  class="{{ Request::is('all-hotel') ? 'active' :'' }} "><a href="{{ route('hotel') }}">Hotel</a></li>
                        <li><a href="contact.html" class="dez-page">About Us</a></li>
                        <li><a href="contact.html" class="dez-page">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- main header END -->
</header>
