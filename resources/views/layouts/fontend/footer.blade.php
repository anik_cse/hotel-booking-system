<footer class="site-footer">
    <div class="footer-top overlay-black-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-5 footer-col-4">
                    <div class="widget widget_getintuch">
                        <h6 class="m-b15 h6 text-uppercase">TRAVEL</h6>
                        <div class="dlab-separator bg-white"></div>
                        <ul class="info-contact">
                            <li>
									<span>
										<i class="fa fa-map-marker	"></i> Brodway Road 234, New York
									</span>
                            </li>

                            <li>
									<span>
										<i class="fa fa-phone"></i> Mobile: +00 1234 456789 <br/>+10 1234 456789
									</span>
                            </li>

                            <li>
									<span>
										<i class="fa fa-envelope-o"></i> Mail: info@travel.com
									</span>
                            </li>
                            <li>
									<span>
										<i class="fa fa-fax"></i> Fax: 000 123 2294 089
									</span>
                            </li>
                        </ul>
                    </div>
                    <ul class="list-inline">
                        <li><a href="javascript:void(0);" class="site-button facebook sharp"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0);" class="site-button google-plus sharp"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="javascript:void(0);" class="site-button linkedin sharp"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="javascript:void(0);" class="site-button twitter sharp"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-7 footer-col-4">
                    <div class="widget  widget_tag_cloud">
                        <h6 class="m-b15 h6 text-uppercase">Tag</h6>
                        <div class="dlab-separator bg-white"></div>
                        <div class="tagcloud">
                            <a href="javascript:void(0);">Design</a>
                            <a href="javascript:void(0);">User interface</a>
                            <a href="javascript:void(0);">SEO</a>
                            <a href="javascript:void(0);">WordPress</a>
                            <a href="javascript:void(0);">Development</a>
                            <a href="javascript:void(0);">Joomla</a>
                            <a href="javascript:void(0);">Design</a>
                            <a href="javascript:void(0);">User interface</a>
                            <a href="javascript:void(0);">SEO</a>
                            <a href="javascript:void(0);">WordPress</a>
                            <a href="javascript:void(0);">Development</a>
                            <a href="javascript:void(0);">Joomla</a>
                            <a href="javascript:void(0);">Design</a>
                            <a href="javascript:void(0);">User interface</a>
                            <a href="javascript:void(0);">SEO</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 footer-col-4">
                    <div class="widget widget_getintuch">
                        <h6 class="m-b15 h6 text-uppercase">Contact us</h6>
                        <div class="dlab-separator bg-white"></div>
                        <div class="search-bx">
                            <div class="dzFormMsg"></div>
                            <form method="post" class="dzForm" action="http://triper.dexignlab.com/xhtml/script/contact.php">
                                <input type="hidden" value="Appointment" name="dzToDo" >
                                <div class="input-group">
                                    <input name="dzName" type="text" required class="form-control" placeholder="Your Name">
                                </div>
                                <div class="input-group">
                                    <input name="dzEmail" type="email" class="form-control" required  placeholder="Your Email Address" >
                                </div>
                                <div class="input-group">
                                    <textarea name="dzMessage" rows="4" class="form-control" required placeholder="Your Message..."></textarea>
                                </div>
                                <div class="input-group">
                                    <button name="submit" type="submit" value="Submit" class="site-button "> <span>Submit</span> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 footer-col-4">
                    <div class="widget widget_gallery">
                        <h6 class="m-b15 h6 text-uppercase">GALLERY</h6>
                        <div class="dlab-separator bg-white"></div>
{{--                        <ul class="clearfix mfp-gallery">--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img1.jpg" title="Title Come Here"><img src="images/gallery/img1.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img2.jpg" title="Title Come Here"><img src="images/gallery/img2.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img3.jpg" title="Title Come Here"><img src="images/gallery/img3.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img4.jpg" title="Title Come Here"><img src="images/gallery/img4.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img5.jpg" title="Title Come Here"><img src="images/gallery/img5.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img6.jpg" title="Title Come Here"><img src="images/gallery/img6.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img7.jpg" title="Title Come Here"><img src="images/gallery/img7.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img8.jpg" title="Title Come Here"><img src="images/gallery/img8.jpg" alt=""></a>--}}
{{--                            </li>--}}
{{--                            <li class="img-effect2">--}}
{{--                                <a class="mfp-link" href="images/gallery/img9.jpg" title="Title Come Here"><img src="images/gallery/img9.jpg" alt=""></a>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer bottom part -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-left"> <span>Copyright © 2019 HBS</span> </div>
                <div class="col-lg-6 col-md-6 text-right "><span> Design With <i class="fa fa-heart text-primary heart"></i> By ITPAL Solutions </span> </div>
            </div>
        </div>
    </div>
</footer>
