@extends('layouts.app')
@section('title',$hotelDetails->hotel_name)
@push('css')
    <style>
        .carousel-item img{
            height: 400px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
{{--        <div class="dlab-bnr-inr overlay-black-middle" style="background-image:url(images/banner/bnr1.jpg);">--}}
{{--            <div class="container">--}}
{{--                <div class="dlab-bnr-inr-entry">--}}
{{--                    <h1 class="text-white">{{ $hotelDetails->hotel_name }}</h1>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- inner page banner END -->
        <div class="content-block">
            <div class="section-full content-inner">
                <div class="container">
                    <div class="row m-b30">
                        <div class="col-lg-12">

                            <div class="product-gallery on-show-slider">
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel45">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="{{ asset('storage/hotel/pic1.jpg') }}" alt="First slide">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="{{ asset('storage/hotel/pic2.jpg') }}" alt="Second slide">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="{{ asset('storage/hotel/pic3.jpg') }}" alt="Third slide">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <div class="d-flex info-bx m-b30" style="margin-top: 20px;">
                                <div class="tour-title">
                                    <h2>{{ $hotelDetails->hotel_name }}</h2>
                                    <p><i class="fa fa-map-marker m-r5"></i>{{ $hotelDetails->city }}, {{ $hotelDetails->zillas }},{{ $hotelDetails->divisions }} </p>
                                </div>
                            </div>
                            <div class="tour-days">

                                <p>{{ $hotelDetails->hotel_profile }}</p>
                            </div>
                        </div>
                        <h3>Rooms</h3>
                            <div class="row packages">

                                    @foreach($rooms as $room)
                                        <div class="col-lg-6 col-xl-3 col-sm-6 col-md-6 m-b30">
                                            <div class="dlab-box">
                                                <div class="dlab-media">
                                                    @php
                                                        $room_image = DB::table('room_image')->where('room_id',$room->id)->first();

                                                    @endphp
                                                    <a href="{{ route('room',[$room->id,$room->room_name]) }}"><img src="{{ asset('storage/room/'.$room_image->image) }}" alt=""></a>
                                                </div>
                                                <div class="dlab-info p-a15 border-1">
                                                    <h4 class="dlab-title m-t0"><a href="{{ route('room',[$room->id,$room->room_name]) }}">{{ $room->room_name }}</a></h4>
                                                    <span class="location">
                                    @php
                                        $hotel = DB::table('hotels')->where('id',$room->hotel_id)->first();

                                    @endphp
                                                        {{ $hotel->zillas }},{{ $hotel->divisions }}
                                </span>
                                                    <div class="package-content">
                                                        <ul class="package-meta">
                                                            <li> <span class="fa fa-user"></span> People: {{ $room->sleep }} </li>
                                                            <li><span class="fa fa-home"></span> Total Room: {{ $room->qty }} </li>

                                                        </ul>
                                                        <div class="clearfix">
                                                            @if($room->booking_sale_price == NULL)
                                                            <span class="package-price pull-left text-primary">৳.{{ $room->booking_price }}</span>
                                                            @else
                                                                <span class=" pull-left text-primary">৳.{{ $room->booking_sale_price }}</span> &nbsp;  ৳.<del>{{ $room->booking_price }}</del>
                                                            @endif
                                                            <button data-toggle="modal" data-target="#bookingNow" class="site-button pull-right"  data-id="{{ $room->id }}" data-qty="{{ $room->qty }}" ><i class="fa fa-location-arrow" aria-hidden="true"></i> Booking Now</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- Booking Modal -->
                                    <div class="modal fade" id="bookingNow" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle"> Room Booking</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="{{ route('booking') }}" method="POST">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                                                <label>Check-in</label>
                                                                <input type="text" id="check_in" name="check_in" value="{{ date('Y-m-d') }}" class="form-control" autocomplete="off"/>
                                                                <script>
                                                                    $('#check_in').datepicker({
                                                                        format: 'yyyy-mm-dd',
                                                                        autoclose: false,
                                                                    });
                                                                </script>
                                                            </div>
                                                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                                                <label for="check_out">Check-out</label>
                                                                <input type="text" id="check_out" name="check_out" value="{{ date('Y-m-d') }}" class="form-control"  autocomplete="off"/>
                                                                <script>
                                                                    $('#check_out').datepicker({
                                                                        format: 'yyyy-mm-dd',
                                                                        autoclose: false,
                                                                    });
                                                                </script>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                                                <label for="username">Name</label>
                                                                <input type="text" class="form-control" id="username" placeholder="Name"/>
                                                                <input type="text" class="form-control" id="id"/>
                                                            </div>
                                                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                                                <label for="email">Email</label>
                                                                <input type="email" class="form-control" id="email" placeholder="email@example.com"/>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                                                <label for="phone">Phone</label>
                                                                <input type="text" class="form-control" pattern="[0-9]{11}" id="phone" placeholder="01234567890"/>
                                                            </div>
                                                            <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                                                <label for="qty">Room Quantity</label>
                                                                <select class="form-control" name="qty">
                                                                    @for ($i = "1"; $i <= $room->qty; $i++)
                                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Booking</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                        </div>

                </div>
            </div>

        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->

@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.carousel45').carousel();
        };

        $('#Edit').on('show.bs.modal', function (event) {
            var button          = $(event.relatedTarget);
            var roomId          = button.data('id');
            var roomQty         = button.data('qty');

            var modal = $(this);

            modal.find('.modal-body #id').val(roomId);
            modal.find('.modal-body #rQty').val(roomQty);



        });
    </script>

@endpush
