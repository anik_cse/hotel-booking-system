<?php

namespace App\Http\Controllers;
use App\Models\Booking;
use App\Models\Hotel;
use App\Models\Room;
use Brian2694\Toastr\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function index()
    {
        $hotels = Hotel::where('is_featured','1')->latest()->paginate(9);
        $rooms = Room::where('is_featured','1')->where('booking_status','0')->latest()->paginate(12);
        return view('welcome',compact('hotels','rooms'));
    }


    // Search
    public function autocomplete(Request $request){


        $data = Room::select("room_name")
            ->where("room_name","LIKE","%{$request->input('keyword')}%")
            ->get();

        return response()->json($data);
    }
    public function searchKeyword(Request $request)
    {
        $checkin        = $request->check_id;
        $checkout       = $request->check_out;
        $search_keyword = $request->keyword;

        $room = Room::where('location_name', 'like', '%' . $search_keyword . '%');
//        $room = Room::where(function($q) use ($checkin,$checkout) {
////            //$q->where('booking_start','!=',$checkin)
////            $q->whereNotBetween('booking_start', [$checkin, $checkout])
////                ->orWhere('booking_start',NULL);
////        })->
////        where(function($q) use ($checkin,$checkout) {
////            //$q->where('booking_end','!=',$checkout)
////            $q->whereNotBetween('booking_start', [$checkout,$checkin])
////                ->orWhere('booking_end',NULL);
////        })->get();
       dd($room);


        //return $room;
    }
    // Hotel
    public function all_hotel()
    {
        $hotels = Hotel::latest()->latest()->paginate(12);
        return view('hotel',compact('hotels'));
    }
    // Hotel Details
     public function hotel($id, $slug)
     {
         $hotelDetails = Hotel::where('id',$id)->first();
         $rooms  = Room::where('hotel_id',$hotelDetails->id)->where('booking_status','0')->orderBy('room_name','ASC')->get();
         return view('hotel_details',compact('hotelDetails','rooms'));
     }

     // Room
    public function room($id, $slug)
    {
        $roomDetails = Room::where('id',$id)->first();
        $roomImages = DB::table('room_image')->where('room_id',$roomDetails->id)->get();
        $facilities = DB::table('room_facilities')->where('room_id',$roomDetails->id)->get();
        $hotelDetails = Hotel::where('id',$roomDetails->hotel_id)->first();
        return view('room',compact('roomDetails','roomImages','facilities','hotelDetails'));
    }
    // One room booking
    public function oneroomBooking(Request $r)
    {
        return $r;
    }
    // Room Booking
    public function roomBooking(Request $request)
    {


        $this->validate($request,[
            'check_in'      => 'required',
            'check_out'     => 'required',
            'qty'           => 'required',
            'username'      => 'required',
            'email'         => 'required',
            'phone'         => 'required',
        ]);
        $rooms = Room::where('id',$request->room_id)->first();

        $booking = new Booking();
        $booking->booking_id    = date('YmdHis');
        $booking->username      = $request->username;
        $booking->email         = $request->email;
        $booking->phone         = $request->phone;
        $booking->hotel_id      = $rooms->hotel_id;
        $booking->room_id       = $request->room_id;
        $booking->booking_date  = date('Y-m-d');
        $booking->booking_start = $request->check_in;
        $booking->booking_end   = $request->check_out;
        $booking->total_amount  = $rooms->booking_price;
        $booking->save();

        $useremail = $request->email;
        $data['title'] = "New Room Booking Has Pending";
        $data['user']  = $request->username;
        $data['phone'] = $request->phone;
        $data['email'] = $request->email;
        $data['user_tilte'] = "congratulations your booking successfully submitted ";
        //Send to admin
        Mail::send('email', $data, function($message)  {

            $message->to('admin@gmail.com', 'Hi Admin')

                ->subject('New Room Booking Has Pending');
        });
        // Send to user
        Mail::send('user_email', $data, function($message) use($useremail)  {

            $message->to($useremail, 'Hi User')

                ->subject('Booking Successfully Submitted');
        });
        //Toastr::success('Room Successfully Updated','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('home');

    }
}
