<?php

namespace App\Http\Controllers\owner;

use App\Models\Facilities;
use App\Models\Hotel;
use App\Models\Owner;
use App\Models\Room;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class RoomController extends Controller
{

    public function index()
    {
        $user_id    = Auth::user()->id;
        $hotel_id   = Hotel::where('user_id',$user_id)->first();
        $rooms      = Room::where('hotel_id',$hotel_id->id)->latest()->get();
        return view('owner.room.index',compact('rooms'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'room_name' => 'required|string',
            'room_size' => 'required',
            'sleeps'    => 'required',
            'booking_price' => 'required',
            'qty'           => 'required',
        ]);
        $hotel          = $request->hotel_name;
        $owner          = Hotel::where('id',$hotel)->first();
        $name           = $request->room_name;
        $size           = $request->room_size;
        $sleep          = $request->sleeps;
        $price          = $request->booking_price;
        $sale_price     = $request->booking_sale_price;
        $qty            = $request->qty;
        $booking_status = $request->booking_status;
        $is_featured    = $request->is_featured;

        $id= DB::table('rooms')->insertGetId(
            array(
            'hotel_id'      => $hotel,
            'user_id'       => $owner->id,
            'room_name'     => $name,
            'room_size'     =>$size,
            'sleep'         =>$sleep,
            'qty'           =>$qty,
            'booking_price' =>$price,
            'booking_sale_price' =>$sale_price,
            'booking_status' =>$booking_status,
            'is_featured'   =>$is_featured
        ));
        // Insert Image
        $files = $request->file('files');
        foreach ($files as $file)
        {
        //$tmp_image = $file->getClientOriginalName();

        // Make Slug
        if(isset($file))
        {
            // Make Unique name
            $currentDate = Carbon::now()->toDateString();
            $image = $currentDate.'-'.uniqid().'.'.$file->getClientOriginalExtension();
            // Check dir exists
            if(!Storage::disk('public')->exists('room'))
            {
                Storage::disk('public')->makeDirectory('room');
            }
            // Resize Image
            $hotelImage = Image::make($file)->resize(1200,900)->save('foo'.$file->getClientOriginalExtension());
            Storage::disk('public')->put('room/'.$image,$hotelImage);
        }
        else{
            $image = "hotel.png";
        }

            DB::table('room_image')->insert(array(
                'room_id' => $id,
                'image'   => $image
            ));
        }
        // Insert Facilities
        $facilities = $request->facilities;
        foreach ($facilities as $facility)
        {
             DB::table('room_facilities')->insert(array(
                'room_id' => $id,
                'facilities_id'   => $facility
            ));
        }

        Toastr::success('Room Successfully Added','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.room.index');
    }
    // Hotel unbooking
    public function unbooking(Request $request)
    {
        $room_id =$request->room_id;
        $room = Room::where('id',$room_id)->first();
        $room->booking_status   = "0";
        $room->booking_start    = NULL;
        $room->booking_end      = NULL;
        $room->save();
        Toastr::success('Room Unbooking Success','Success');
        return redirect()->back();
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $r_id = $request->id;
        $check_in = $request->check_in;
        $check_out = $request->check_out;
        $room = Room::where('id',$r_id)->first();
        $room->booking_start    = $check_in;
        $room->booking_end      = $check_out;
        $room->booking_status   = "1";
        $room->save();
        Toastr::success('Room Boooking Success ','Succcess');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $room = Room::where('id',$id)->first();
        $roomImages = DB::table('room_image')->where('room_id',$room->id)->get();

        foreach($roomImages as $image)
        {
            if(Storage::disk('public')->exists('room/'.$image->image))
            {
                Storage::disk('public')->delete('room/'.$image->image);
            }
        }

        $room->delete();
        Toastr::success('Room Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.room.index');

    }

}
