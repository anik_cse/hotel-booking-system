<?php

namespace App\Http\Controllers\owner;

use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{

    public function index()
    {
        $user_id = Auth::user()->id;
        $profile = User::where('id',$user_id)->first();
        return view('owner.profile_setting',compact('profile'));
    }
    // View Profile
    public function show($id)
    {
        $posts = User::where('id',Auth::id())->get();
        return view('owner.profile',compact('posts'));
    }


    public function update(Request $request)
    {
        $this->validate($request,[
            'name'  => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|max:255',
            //'about' => 'required',
            'image' => 'image',
        ]);
        $user = User::findOrFail(Auth::id());

        $image = $request->file('image');
        if(isset($image))
        {
            //Make unique image name
            $currentDate =  Carbon::now()->toDateString();
            $imageName   = $currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            //Check Post Dir is exists
            if(!Storage::disk('public')->exists('profile'))
            {
                Storage::disk('public')->makeDirectory('profile');
            }
            //Delete old post Image
            if(Storage::disk('public')->exists('profile/'.$user->image))
            {
                Storage::disk('public')->delete('profile/'.$user->image);
            }
            // Resize image for post and upload
            $postImage = Image::make($image)->resize(250,250)->save('foo' . $image->getClientOriginalExtension());
            Storage::disk('public')->put('profile/'.$imageName,$postImage);
        }
        else{
            $imageName = $user->image;
        }
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->phone    = $request->phone;
        $user->image    = $imageName;
        //$user->about    = $request->about;
        $user->update();

        Toastr::success('Profile Successfully Updated','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('owner.profile.index');


    }
    // Update Password
    public function passwordUpdate(Request $request)
    {
        $this->validate($request,[
            'old_password'  => 'required',
            'password'      => 'required|confirmed',
        ]);

        $hashedPassword = Auth::user()->password;
        if(Hash::check($request->old_password,$hashedPassword))
        {
            if(!Hash::check($request->password,$hashedPassword))
            {
                $user = User::findOrFail(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();
                Toastr::info('Password Successfully Changed','Info',["positionClass"=>"toast-top-right"]);
                Auth::logout();
                return redirect()->back();
            }
            else{
                Toastr::error('New password cannot be the same as old password','Error',["positionClass"=>"toast-top-right"]);
                return redirect()->back();
            }
        }
        else{
            Toastr::error('Current password not match','Error',["positionClass"=>"toast-top-right"]);
            return redirect()->back();
        }
    }

}
