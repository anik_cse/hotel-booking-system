<?php

namespace App\Http\Controllers\admin;

use App\Models\Facilities;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FacilitiesController extends Controller
{

    public function index()
    {
        $facilities = Facilities::latest()->get();
        return view('admin.facilities.index',compact('facilities'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        $facilities = $request->facilities;
        foreach ($facilities as $facility)
        {
            DB::table('facilities')->insert(array(
                'name'   => $facility
            ));
        }
        Toastr::success('Facilities Added Successfully','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.facilities.index');
    }


    public function show(Facilities $facilities)
    {
        //
    }


    public function edit(Facilities $facilities)
    {
        //
    }

    public function update(Request $request)
    {
        $facilities_id  = $request->id;
        $name           = $request->faciname;
        $facilities     = Facilities::where('id',$facilities_id)->first();
        $facilities->name = $name;
        $facilities->save();

        Toastr::success('Facilities Successfully Updated','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.facilities.index');
    }

    public function destroy($id)
    {
        $facilities = Facilities::where('id',$id)->first();
        $facilities->delete();
        Toastr::success('Facilities Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.facilities.index');
    }
}
