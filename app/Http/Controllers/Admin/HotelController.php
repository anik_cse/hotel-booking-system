<?php

namespace App\Http\Controllers\admin;

use App\Models\Division;
use App\Models\Hotel;
use App\Models\Zilla;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class HotelController extends Controller
{

    public function index()
    {
        $owners     = User::where('role_id','2')->where('status','1')->get();
//        $divisions   = Division::orderBy('division_name','ASC')->get();
//        $zilla      = Zilla::orderBy('zilla_name','ASC')->get();
        $hotels     = Hotel::latest()->get();
        return view('admin.hotel.index',compact('owners','hotels'));

    }

    public function getDistrict(Request $r){

//        if($r->has('division_id')){
//            return DB::table('zillas')->where('division_id',$r->input('division_id'))->get();
//
//        }
//        else{
//            echo " not found";
//        }
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'hotel_name' => 'required|string|max:255',
            'division' => 'required|string|max:255',
            'district' => 'required|string|max:255',
            'owner' => 'required|string|max:255',
            'about' => 'required|string',
            'image' => 'required|image',
        ]);

        $image = $request->file('image');
        // Make Slug
        function make_slug($string)
        {
            return preg_replace('/\s+/u','-',trim($string));
        }
        if(isset($image))
        {
            // Make Unique name
            $currentDate = Carbon::now()->toDateString();
            $imageName = $currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            // Check dir exists
            if(!Storage::disk('public')->exists('hotel'))
            {
                Storage::disk('public')->makeDirectory('hotel');
            }
            // Resize Image
            $hotelImage = Image::make($image)->resize(1200,900)->save('foo'.$image->getClientOriginalExtension());
            Storage::disk('public')->put('hotel/'.$imageName,$hotelImage);
        }
        else{
            $imageName = "hotel.png";
        }
        $slug = make_slug($request->hotel_name);
        $hotel                  = new Hotel();
        $hotel->hotel_name      = $request->hotel_name;
        $hotel->user_id         = $request->owner;
        $hotel->slug            = $slug;
        $hotel->divisions       = $request->division;
        $hotel->zillas          = $request->district;
        $hotel->city            = $request->area;
        $hotel->is_featured     = $request->is_featured;
        $hotel->hotel_profile   = $request->about;
        $hotel->hotel_image     = $imageName;
        $hotel->save();
        Toastr::success('Hotel Successfully Added','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.hotel.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'hotel_name'    => 'required|string|max:255',
            'division'      => 'required|string|max:255',
            'district'      => 'required|string|max:255',
            'owner'         => 'required|string|max:255',
            'about'         => 'required|string',
        ]);
         $hId = $request->id;
        $hotel = Hotel::where('id',$hId)->first();
        $image = $request->file('image');
        // Make Slug
        function make_slug($string)
        {
            return preg_replace('/\s+/u','-',trim($string));
        }
        if(isset($image))
        {
            // Make Unique name
            $currentDate = Carbon::now()->toDateString();
            $imageName = $currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            // Check dir exists
            if(!Storage::disk('public')->exists('hotel'))
            {
                Storage::disk('public')->makeDirectory('hotel');
            }
            //Delete old post Image
            if(Storage::disk('public')->exists('hotel/'.$hotel->image))
            {
                Storage::disk('public')->delete('hotel/'.$hotel->image);
            }
            // Resize Image
            $hotelImage = Image::make($image)->resize(1200,900)->save('foo'.$image->getClientOriginalExtension());
            Storage::disk('public')->put('hotel/'.$imageName,$hotelImage);
        }
        else{
            $imageName = $hotel->hotel_image;
        }
        $slug = make_slug($request->hotel_name);

        $hotel->hotel_name  = $request->hotel_name;
        $hotel->user_id     = $request->owner;
        $hotel->slug        = $slug;
        $hotel->divisions   = $request->division;
        $hotel->zillas      = $request->district;
        $hotel->city        = $request->area;
        $hotel->is_featured     = $request->is_featured;
        $hotel->hotel_profile   = $request->about;
        $hotel->hotel_image     = $imageName;
        $hotel->save();
        Toastr::success('Hotel Successfully Update','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.hotel.index');
    }


    public function destroy($id)
    {
       $hotel = Hotel::where('id',$id)->first();
       if($hotel->image == "hotel.png")
       {
           $hotel->delete();
           Toastr::success('Hotel Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
           return redirect()->route('admin.hotel.index');
       }
       else{
           if(Storage::disk('public')->exists('hotel/'.$hotel->image))
           {
               Storage::disk('public')->delete('hotel/'.$hotel->image);
           }
           $hotel->delete();
           Toastr::success('Hotel Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
           return redirect()->route('admin.hotel.index');
       }
    }
}
