<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Owner;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OwnerController extends Controller
{

    public function index()
    {
        $owners = User::where('role_id','2')->latest()->get();
        return view('admin.owner.index',compact('owners'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'phone' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
        ]);


        $owner = new User();
        $owner->name    = $request->name;
        $owner->role_id = "2";
        $owner->phone   = $request->phone;
        $owner->email   = $request->email;
        $owner->status  = "1";
        $owner->password = bcrypt('123456');
        $owner->save();

        Toastr::success('Hotel Owner Added Successfully','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.owner.index');
    }


    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function edit(Owner $owner)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'phone' => 'required',
            'email' => 'required|string|email|max:255',
        ]);

        $ownerid = $request->id;

        $owner =  User::where('id',$ownerid)->first();
        $owner->name    = $request->name;
        $owner->phone   = $request->phone;
        $owner->email   = $request->email;
        $owner->status  = $request->status;
        $owner->save();

        Toastr::success('Hotel Owner Successfully Update','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.owner.index');
    }


    public function destroy($id)
    {
        $owner = User::where('id',$id)->first();
        if($owner->image == "user.png"){
            $owner->delete();
            Toastr::success('Hotel Owner Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
            return redirect()->route('admin.owner.index');
        }
        else{
            if(Storage::disk('public')->exists('profile/'.$owner->image))
            {
                Storage::disk('public')->delete('profile/'.$owner->image);
            }
            $owner->delete();
            Toastr::success('Hotel Owner Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
            return redirect()->route('admin.owner.index');
        }
    }
}
