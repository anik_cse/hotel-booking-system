<?php

namespace App\Http\Controllers\admin;

use App\Models\Division;
use App\Models\Zilla;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZillaController extends Controller
{

    public function index()
    {
        $divisions      = Division::orderBy('division_name','ASC')->get();
        $zillas         = Zilla::latest()->get();
        return view('admin.zilla.index',compact('divisions','zillas'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'division_id' => 'required',
        ]);
        // Make Slug
        function make_slug($string) {
            return preg_replace('/\s+/u', '-', trim($string));
        }
        $slug = make_slug($request->name);

        $zilla = new Zilla();
        $zilla->zilla_name  = $request->name;
        $zilla->slug        = $slug;
        $zilla->division_id = $request->division_id;
        $zilla->save();
        Toastr::success('Zilla Successfully Added','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.zilla.index');



    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'division_id' => 'required',
        ]);
        // Make Slug
        function make_slug($string) {
            return preg_replace('/\s+/u', '-', trim($string));
        }
        $slug   = make_slug($request->name);
        $zId    = $request->id;
        $zilla  = Zilla::where('id',$zId)->first();

        $zilla->zilla_name  = $request->name;
        $zilla->slug        = $slug;
        $zilla->division_id = $request->division_id;
        $zilla->save();
        Toastr::success('Zilla Successfully Updated','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.zilla.index');
    }

    public function destroy($id)
    {
        $zilla = Zilla::where('id',$id)->first();
        $zilla->delete();
        Toastr::success('Zilla Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.zilla.index');
    }
}
