<?php

namespace App\Http\Controllers\admin;

use App\Models\Facilities;
use App\Models\Hotel;
use App\Models\Owner;
use App\Models\Room;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class RoomController extends Controller
{

    public function index()
    {
        $hotels     = Hotel::orderBy('hotel_name','ASC')->get();
        $facilities = Facilities::orderBy('name','ASC')->get();
        $rooms      = Room::latest()->get();
        if($rooms->count() == "0")
        {
            return view('admin.room.index',compact('hotels','facilities','rooms'));
        }
        else{
            foreach ($rooms as $room)
            {
                $room_faci = DB::table('room_facilities')->where('room_id',$room->id)->get();
                foreach ($room_faci as $singfaci)
                {
                    $faci_id = $singfaci->facilities_id;
                    //dd($faci_id);
                }

            }

            return view('admin.room.index',compact('hotels','facilities','rooms','faci_id'));
        }

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'room_name' => 'required|string',
            'room_size' => 'required',
            'sleeps'    => 'required',
            'booking_price' => 'required',
            'qty'           => 'required',
        ]);
        $hotel          = $request->hotel_name;
        $owner          = Hotel::where('id',$hotel)->first();
        $name           = $request->room_name;
        $size           = $request->room_size;
        $sleep          = $request->sleeps;
        $price          = $request->booking_price;
        $sale_price     = $request->booking_sale_price;
        $qty            = $request->qty;
        $booking_status = $request->booking_status;
        $is_featured    = $request->is_featured;

        // Insert into room table
        $id= DB::table('rooms')->insertGetId(
            array(
                'hotel_id'      => $hotel,
                'user_id'       => $owner->id,
                'room_name'     => $name,
                'room_size'     =>$size,
                'sleep'         =>$sleep,
                'qty'           =>$qty,
                'booking_price' =>$price,
                'booking_sale_price' =>$sale_price,
                'booking_status' =>$booking_status,
                'is_featured'   =>$is_featured
            ));

        // Insert Image
        $files = $request->file('files');
        foreach ($files as $file)
        {
        //$tmp_image = $file->getClientOriginalName();

        // Make Slug
        if(isset($file))
        {
            // Make Unique name
            $currentDate = Carbon::now()->toDateString();
            $image = $currentDate.'-'.uniqid().'.'.$file->getClientOriginalExtension();
            // Check dir exists
            if(!Storage::disk('public')->exists('room'))
            {
                Storage::disk('public')->makeDirectory('room');
            }
            // Resize Image
            $hotelImage = Image::make($file)->resize(1200,900)->save('foo'.$file->getClientOriginalExtension());
            Storage::disk('public')->put('room/'.$image,$hotelImage);



            DB::table('room_image')->insert(array(
                'room_id' => $id,
                'image'   => $image
            ));
        }
        else{
            $image = "hotel.png";
            DB::table('room_image')->insert(array(
                'room_id' => $id,
                'image'   => $image
            ));
        }


        }
        // Insert Facilities
        $facilities = $request->facilities;
        foreach ($facilities as $facility)
        {
             DB::table('room_facilities')->insert(array(
                'room_id' => $id,
                'facilities_id'   => $facility
            ));
        }

        Toastr::success('Room Successfully Added','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.room.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'room_name'     => 'required|string',
            'room_size'     => 'required',
            'sleeps'        => 'required',
            'booking_price' => 'required',
            'qty'           => 'required',
        ]);
        $files = $request->file('files');
        $room_id = $request->room_id;

        $roomImages = DB::table('room_image')->where('room_id',$room_id)->get();

        $hotel          = $request->hotel_name;
        $owner          = Hotel::where('id',$hotel)->first();
        $name           = $request->room_name;
        $size           = $request->room_size;
        $sleep          = $request->sleeps;
        $price          = $request->booking_price;
        $sale_price     = $request->booking_sale_price;
        $qty            = $request->qty;
        $booking_status = $request->booking_status;
        $is_featured    = $request->is_featured;

        // Insert into room table
        Room::where('id',$room_id)->update(array(
            'hotel_id'      => $hotel,
            'user_id'       => $owner->id,
            'room_name'     => $name,
            'room_size'     =>$size,
            'sleep'         =>$sleep,
            'qty'           =>$qty,
            'booking_price' =>$price,
            'booking_sale_price' =>$sale_price,
            'booking_status' =>$booking_status,
            'is_featured'   =>$is_featured
        ));

        foreach ($files as $file)
        {
            if(isset($file))
            {
                // Make Unique name
                $currentDate = Carbon::now()->toDateString();
                $image = $currentDate.'-'.uniqid().'.'.$file->getClientOriginalExtension();
                // Check dir exists
                if(!Storage::disk('public')->exists('room'))
                {
                    Storage::disk('public')->makeDirectory('room');
                }
                // Delete old image
                if($roomImages->count() == NULL)
                {

                }
                else{
                    foreach ($roomImages as $room_image)
                    {
                        //Delete old post Image
                        if(Storage::disk('public')->exists('room/'.$room_image->image))
                        {
                            Storage::disk('public')->delete('room/'.$room_image->image);
                        }
                    }
                    // Resize Image
                    $hotelImage = Image::make($file)->resize(1200,900)->save('foo'.$file->getClientOriginalExtension());
                    Storage::disk('public')->put('room/'.$image,$hotelImage);

                    DB::table('room_image')->where('room_id',$room_id)->update(array(
                        'image'   => $image
                    ));
                }

            }
            else{

            }
        }

        Toastr::success('Room Successfully Updated','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.room.index');


    }

    public function destroy($id)
    {
        $room = Room::where('id',$id)->first();
        $roomImages = DB::table('room_image')->where('room_id',$room->id)->get();

        foreach($roomImages as $image)
        {
            if(Storage::disk('public')->exists('room/'.$image->image))
            {
                Storage::disk('public')->delete('room/'.$image->image);
            }
        }

        $room->delete();
        Toastr::success('Room Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.room.index');

    }

}
