<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Room;
use App\Notifications\BookingConfirmed;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class BookingController extends Controller
{

    public function index()
    {
        $newBookings        = Booking::where('booking_status',"0")->latest()->get();
        return view('admin.booking.index',compact('newBookings'));
    }

    public function create()
    {
        $CompleteBooking    = Booking::where('booking_status',"1")->latest()->get();
        return view('admin.booking.create',compact('CompleteBooking'));
    }

    // Reject booking
    public function bookingReject()
    {
        $rejectBooking    = Booking::where('booking_status',"2")->latest()->get();
        return view('admin.booking.reject',compact('rejectBooking'));
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Booking $booking)
    {
        //
    }

    public function edit(Booking $booking)
    {
        //
    }

    public function update(Request $request, $id)
    {

        $booking_id     = $request->id;
        $book_id     = $request->Bookid;
        $booking_status = $request->status;
        $checkin        = $request->check_in;
        $checkout       = $request->check_out;
        $room_id        = $request->room_id;
        $room_name      = $request->room_name;
        $hotel_name     = $request->hotel_name;
        $username       = $request->username;
        $phone          = $request->phone;
        $amount         = $request->amount;
        $booking_email  = $request->email;

        $booking = Booking::where('id',$booking_id)->first();
        $booking->booking_status = $booking_status;
        $booking->save();




        // Update Room information
        if($booking_status == "1")
        {
            $room = Room::where('id',$room_id)->first();
            $room->booking_start    = $checkin;
            $room->booking_end      = $checkout;
            $room->booking_status   ="1";
            $room->save();
            // Send Email to user
            $data['title']      = "Congratulations your booking is comfirmed .";
            $data['username']   = $username;
            $data['phone']      = $phone;
            $data['room']       = $room_name;
            $data['hotel']      = $hotel_name;
            $data['check_in']   = $checkin;
            $data['check_out']  = $checkout;
            $data['booking_id'] = $book_id;
            $data['amount']     = $amount;

            Mail::send('admin.booking.email', $data, function($message) use($booking_email,$username) {

                $message->to($booking_email, $username)

                    ->subject('Booking Confirmed');
            });

            Toastr::success('Booking Successfully Confirmed','Success');
            return redirect()->route('admin.booking.index');

        }
        else
        {
            $room = Room::where('id',$room_id)->first();
            $room->booking_start    = $checkin;
            $room->booking_end      = $checkout;
            $room->booking_status   ="2";
            $room->save();
            // Send Email to user
            $data['title']      = "Opps !! your booking has been rejected.";

            Mail::send('admin.booking.reject_email', $data, function($message) use($booking_email,$username) {

                $message->to($booking_email, $username)

                    ->subject('Booking has been rejected');
            });

            Toastr::success('Booking Successfully Rejected','Success');
            return redirect()->route('admin.booking.index');

        }

        //$hotel_admin = User::where('role_id',"1")->get();
        //$hotel_owner = User::where('role_id',"2")->get();
        //$booking_email  = $request->email;
        //$booking_email  = Booking::where('room_id',$room_id)->select('email')->first();

        // Send email to user
        //Notification::send($booking_email, new BookingConfirmed($booking));
        //$data['title'] = "This is Test Mail Tuts Make";
        //$data['user']  = Booking::where('')

//        Mail::send('admin.booking.email', $data, function($message) use($booking_email) {
//
//            $message->to($booking_email, 'Receiver Name')
//
//                ->subject('Booking Confirmed');
//        });
        // owner email send
//        Mail::send('admin.booking.email', $data, function($message) use($hotel_owner) {
//
//            $message->to($hotel_owner, 'Receiver Name')
//
//                ->subject('Booking Confirmed');
//        });

//        if (Mail::failures()) {
//            return response()->Fail('Sorry! Please try again latter');
//        }else{
//            return response()->success('Great! Successfully send in your mail');
//        }



    }

    public function destroy(Booking $booking)
    {
        $booking->delete();
        Toastr::success('Booking Successfully Deleted','Success');
        return redirect()->route('admin.booking.index');
    }
}
