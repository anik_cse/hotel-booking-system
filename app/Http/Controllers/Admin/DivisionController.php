<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Models\Division;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class DivisionController extends Controller
{

    public function index()
    {
        $divisions = Division::latest()->get();
        return view('admin.division.index',compact('divisions'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255'
        ]);
        // Make Slug
        function make_slug($string) {
            return preg_replace('/\s+/u', '-', trim($string));
        }
        $slug = make_slug($request->name);

        $division = new Division();
        $division->division_name = $request->name;
        $division->slug = $slug;
        $division->save();

        Toastr::success('Division Successfully Added','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.division.index');
    }

    public function show(Division $division)
    {
        //
    }

    public function edit(Division $division)
    {
        //
    }

    public function update(Request $request,$id)
    {

        $this->validate($request,[
            'name' => 'required|string|max:255'
        ]);
        // Make Slug
        function make_slug($string) {
            return preg_replace('/\s+/u', '-', trim($string));
        }
        $slug = make_slug($request->name);
        $divId = $request->id;
        $division = Division::where('id',$divId)->first();
        $division->division_name = $request->name;
        $division->slug = $slug;
        $division->save();

        Toastr::success('Division Successfully Updated','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.division.index');
    }


    public function destroy(Division $division)
    {
        $division->delete();
        Toastr::success('Division Successfully Deleted','Success',["positionClass"=>"toast-top-right"]);
        return redirect()->route('admin.division.index');
    }
}
