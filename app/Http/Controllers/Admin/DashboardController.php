<?php

namespace App\Http\Controllers\admin;

use App\Models\Booking;
use App\Models\Division;
use App\Models\Facilities;
use App\Models\Hotel;
use App\Models\Room;
use App\Models\Zilla;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index()
    {
        $owners     = User::where('role_id','2')->get();
        $hotels     = Hotel::latest()->get();
        $rooms      = Room::latest()->get();
        $complete_booking   = Booking::where('booking_status',"1")->get();
        $pending_booking    = Booking::where('booking_status',"0")->get();
        $facilities = Facilities::latest()->get();
       return view('admin.dashboard',compact('owners','hotels','rooms','facilities','complete_booking','pending_booking'));
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
