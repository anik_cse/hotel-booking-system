<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    // relationship with room
    public function room()
    {
        return $this->belongsTo('App\Models\Room');
    }
    // relationship with hotel
    public function hotel()
    {
       return $this->belongsTo('App\Models\Hotel');
    }
}
