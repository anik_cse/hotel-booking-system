<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    // Relationship with hotels
    public function hotels()
    {
        //return $this->belongsTo('App\Models\Hotel');
        return $this->hasMany('App\Models\Hotel');
    }
}
