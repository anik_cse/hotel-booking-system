<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    // relationship with room
    public function facilities()
    {
        return $this->hasMany('App\Models\Facilities');
    }

    // relationship with booking
    public function bookings()
    {
        return $this->hasMany('App\Models\Booking');
    }

    // relationship with hotel
    public function  hotel()
    {
        return $this->belongsTo('App\Models\Hotel');
    }
}
