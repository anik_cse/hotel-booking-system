<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    // relationship with owner
    public function owner()
    {
        return $this->hasMany('App\User');
        //return $this->belongsTo('App\User');
    }
    // relationship with booking
    public function bookings()
    {
        return $this->hasMany('App\Models\Booking');
    }
    // relationship with room
    public function rooms()
    {
        return $this->hasMany('App\Models\Room');
    }
}
