<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    // Relationship with Zilla
    public function zillas()
    {
        return $this->hasMany('App\Models\Zilla');
    }
}
