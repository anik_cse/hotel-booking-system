<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zilla extends Model
{
    // Relationship with Zilla
    public function division()
    {
        return $this->belongsTo('App\Models\Division');
    }
}
