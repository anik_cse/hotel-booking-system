<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facilities extends Model
{
    // relationship with facilities
    public function facilities()
    {
        return $this->belongsTo('App\Models\Room');
    }
}
