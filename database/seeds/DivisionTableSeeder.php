<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisions')->insert([
            'division_name' => 'Barisal',
            'slug' => 'barisal',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('divisions')->insert([
            'division_name' => 'Chittagong',
            'slug' => 'chittagong',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('divisions')->insert([
            'division_name' => 'Dhaka',
            'slug' => 'dhaka',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('divisions')->insert([
            'division_name' => 'Khulna',
            'slug' => 'khulna',
            'created_at' => date('Y-m-d'),
        ]);
    }
}
