<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@hotel.com',
            'phone' => '01234567891',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'owner',
            'username' => 'owner',
            'email' => 'owner@hotel.com',
            'phone' => '01234567891',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'user',
            'username' => 'user',
            'email' => 'user@hotel.com',
            'phone' => '01234567891',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d'),
        ]);
    }
}
