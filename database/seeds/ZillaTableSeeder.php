<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ZillaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zillas')->insert([
            'division_id' => '1',
            'zilla_name' => 'Barguna',
            'slug' => 'barguna',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('zillas')->insert([
            'division_id' => '1',
            'zilla_name' => 'Barisal',
            'slug' => 'barisal',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('zillas')->insert([
            'division_id' => '1',
            'zilla_name' => 'Bhola',
            'slug' => 'bhola',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('zillas')->insert([
            'division_id' => '2',
            'zilla_name' => 'Bandarban',
            'slug' => 'bandarban',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('zillas')->insert([
            'division_id' => '2',
            'zilla_name' => 'Brahmanbaria',
            'slug' => 'brahmanbaria',
            'created_at' => date('Y-m-d'),
        ]);
    }
}
