<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            'zillas_id' => '1',
            'city_name' => 'Amtali',
            'slug' => 'amtali',
            'created_at' => date('Y-m-d'),
        ]);
        DB::table('cities')->insert([
            'zillas_id' => '1',
            'city_name' => 'Bamna',
            'slug' => 'bamna',
            'created_at' => date('Y-m-d'),
        ]);

        DB::table('cities')->insert([
            'zillas_id' => '1',
            'city_name' => 'Betagi',
            'slug' => 'betagi',
            'created_at' => date('Y-m-d'),
        ]);
    }
}
