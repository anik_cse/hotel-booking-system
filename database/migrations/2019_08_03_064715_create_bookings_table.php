<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',30);
            $table->string('email')->unique();
            $table->string('phone',20);
            $table->integer('hotel_id')->unsigned();;
            $table->integer('room_id')->unsigned();;
            $table->date('booking_date');
            $table->date('booking_start');
            $table->date('booking_end');
            $table->boolean('booking_status')->default(false);
            $table->float('total_amount',8,2);
            $table->string('payment_type')->default('cash');
            $table->foreign('hotel_id')
                ->references('id')->on('hotels')
                ->onDelete('cascade');
            $table->foreign('room_id')
                ->references('id')->on('rooms')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
