<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/search','HomeController@searchKeyword')->name('search');
Route::get('/autocomplete', 'HomeController@autocomplete')->name('autocomplete');
// Hotel
Route::get('/all-hotel','HomeController@all_hotel')->name('hotel');
// Hotel Details
Route::get('/hotel/{id}/{slug}','HomeController@hotel')->name('hotel-details');
// Room
Route::get('/room/{id}/{slug}','HomeController@room')->name('room');
// Room Booking
Route::post('/room_booking','HomeController@roomBooking')->name('booking');
Route::post('/one-room-booking','HomeController@oneroomBooking')->name('one_booking');
// Admin Route
Route::group(['as' =>'admin.','prefix'=>'admin','namespace'=>'admin','middleware' => ['auth','admin']],function (){

    // Dashboard Route
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    // Owner
    Route::resource('owner','OwnerController');
    // Division
    Route::resource('division','DivisionController');
    // Zillas
    Route::resource('zilla','ZillaController');
    // Hotel
    Route::resource('hotel','HotelController');
    // Facilities
    Route::resource('facilities','FacilitiesController');

    Route::get('/district','HotelController@getDistrict');
   // Room
    Route::resource('room','RoomController');
    // Booking
    Route::resource('booking','BookingController');
    Route::get('/reject','BookingController@bookingReject')->name('booking.reject');



    // Profile Route
    Route::get('profile','ProfileController@index')->name('profile.index');
    Route::get('view-profile/{id}','ProfileController@show')->name('profile.view');
    Route::put('update-profile','ProfileController@update')->name('profile.update');
    Route::put('change-password','ProfileController@passwordUpdate')->name('password.update');
});
// Owner Route
Route::group(['as' =>'owner.','prefix'=>'owner','namespace'=>'owner','middleware' => ['auth','owner']],function (){

    // Dashboard Route
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    // Room
    Route::resource('room','RoomController');
    Route::post('/room-free','RoomController@unbooking')->name('room-free');

    // Profile Route
    Route::get('profile','ProfileController@index')->name('profile.index');
    Route::get('view-profile/{id}','ProfileController@show')->name('profile.view');
    Route::put('update-profile','ProfileController@update')->name('profile.update');
    Route::put('change-password','ProfileController@passwordUpdate')->name('password.update');
});
// User Route
Route::group(['as' =>'user.','prefix'=>'user','namespace'=>'user','middleware' => ['auth','user']],function (){

    // Dashboard Route
    Route::get('/dashboard','DashboardController@index')->name('dashboard');

    // Profile Route
    Route::get('profile','ProfileController@index')->name('profile.index');
    Route::get('view-profile/{id}','ProfileController@show')->name('profile.view');
    Route::put('update-profile','ProfileController@update')->name('profile.update');
    Route::put('change-password','ProfileController@passwordUpdate')->name('password.update');
});
